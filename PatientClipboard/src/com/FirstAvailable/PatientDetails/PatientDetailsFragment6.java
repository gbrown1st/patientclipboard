package com.FirstAvailable.PatientDetails;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.FirstAvailable.PatientDetails.PatientDetailsFragment4.DateDialogFragmentListener;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class PatientDetailsFragment6 extends Fragment {
	
	public static final String PREFS_NAME = "MyPrefsFile";
	public static SharedPreferences settings = null;
	public Spinner pensionsSpinner;
	public JSONArray pensions_array;
	public static String surgery_id;
	
	DateDialogFragment frag;
	Button button;
	Button button2;
    Calendar now;
    
    SharedPreferences.Editor editor;
	
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {
	        return inflater.inflate(R.layout.patient_details_6, container, false);
	    }

		@Override
	    public void onStop() {
	    // TODO Auto-generated method stub
	        super.onStop();

	        Log.i("onStop", "stopping step 6");
	        
	        EditText mn = ((EditText) getView().findViewById(R.id.medicare_number));
		    EditText mln = ((EditText) getView().findViewById(R.id.medicare_line_number));
		    EditText pn = ((EditText) getView().findViewById(R.id.pension_number));
		    
		    String medicare_number = mn.getText().toString();
			String medicare_line_number = mln.getText().toString();
			String pension_number = pn.getText().toString();
			
			editor.putString("medicare_number", medicare_number);
			editor.putString("medicare_line_number", medicare_line_number);
			editor.putString("pension_number", pension_number);
			
			editor.commit();
			
			Log.i("medicare_number", " - " + settings.getString("medicare_number", ""));
			Log.i("medicare_line_number", " - " + settings.getString("medicare_line_number", ""));
			 
		}
		
		@Override
		public void onViewCreated(View view, Bundle savedInstanceState) {
	
		    super.onViewCreated(view, savedInstanceState);
		    
		    now = Calendar.getInstance();
		    
		    settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
		    editor = settings.edit();
		    
			surgery_id = settings.getString("surgery_id", "");
			
			pensionsSpinner = (Spinner) getView().findViewById(R.id.pensions_spinner);
			
			String pensions_json = settings.getString("pensions_json", "");
			Log.i("pensions_json", pensions_json);
			
			if (pensions_json.length() == 0) {
				getPensions();
		    } else {
		    	loadPensionsJSONResult(pensions_json);
		    }
			
			String medicare_number = settings.getString("medicare_number", "");
			String medicare_line_number = settings.getString("medicare_line_number", "");
			String medicare_expiry = settings.getString("medicare_expiry", "");
			
			String[] separated = medicare_expiry.split("-");
				String month = separated[1];
				String year = separated[0];
			
			if (month.equals("01") || month.equals("1")) {
				month = "January";
			} else if (month.equals("02") || month.equals("2")) {
				month = "February";
			} else if (month.equals("03") || month.equals("3")) {
				month = "March";
			} else if (month.equals("04") || month.equals("4")) {
				month = "April";
			} else if (month.equals("05") || month.equals("5")) {
				month = "May";
			} else if (month.equals("06") || month.equals("6")) {
				month = "June";
			} else if (month.equals("07") || month.equals("7")) {
				month = "July";
			} else if (month.equals("08") || month.equals("8")) {
				month = "August";
			} else if (month.equals("09") || month.equals("9")) {
				month = "September";
			} else if (month.equals("10")) {
				month = "October";
			} else if (month.equals("11")) {
				month = "November";
			} else if (month.equals("12")) {
				month = "December";
			}
			
	        button = (Button) getView().findViewById(R.id.medicare_expiry_btn);
	        button.setText(month + " " + year);
	        button.setOnClickListener(new View.OnClickListener() {
	        	public void onClick(View v) {
	        		showDialog();	
	        	}
	        });
			
			EditText mn = ((EditText) getView().findViewById(R.id.medicare_number));
			mn.setText(medicare_number);
		    EditText mln = ((EditText) getView().findViewById(R.id.medicare_line_number));
		    mln.setText(medicare_line_number);
		    
		    String pension_number = settings.getString("pension_number", "");
			String pension_expiry = settings.getString("pension_expiry", "");
			
			String pe = "";
			separated = pension_expiry.split("-");
			month = separated[1];
			
			if (pension_expiry == "1900-01-01") {
				pe = "";
			} else {
				
				if (month.equals("01") || month.equals("1")) {
					month = "January";
				} else if (month.equals("02") || month.equals("2")) {
					month = "February";
				} else if (month.equals("03") || month.equals("3")) {
					month = "March";
				} else if (month.equals("04") || month.equals("4")) {
					month = "April";
				} else if (month.equals("05") || month.equals("5")) {
					month = "May";
				} else if (month.equals("06") || month.equals("6")) {
					month = "June";
				} else if (month.equals("07") || month.equals("7")) {
					month = "July";
				} else if (month.equals("08") || month.equals("8")) {
					month = "August";
				} else if (month.equals("09") || month.equals("9")) {
					month = "September";
				} else if (month.equals("10")) {
					month = "October";
				} else if (month.equals("11")) {
					month = "November";
				} else if (month.equals("12")) {
					month = "December";
				}
				String dayString = separated[2];
				if (dayString.length() == 2 && dayString.substring(0) == "0") {
					dayString = dayString.substring(1);
				}
				
				pe = dayString + " " + month + " " + separated[0];
			}
			
			button2 = (Button) getView().findViewById(R.id.pension_expiry_btn);
	        button2.setText(pe);
	        button2.setOnClickListener(new View.OnClickListener() {
	        	public void onClick(View v) {
	        		showDialog2();	
	        	}
	        });
			
			EditText pn = ((EditText) getView().findViewById(R.id.pension_number));
			pn.setText(pension_number);
		    
		    Log.i("medicare_number", " - " + medicare_number);
		    Log.i("medicare_line_number", " - " + medicare_line_number);
		    
		    pensionsSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
	    	    @Override
	    	    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
	    
					try {
						JSONObject selectedTitle = pensions_array.getJSONObject(position);
						String pension = selectedTitle.getString("pension_type");
						String pension_code = selectedTitle.getString("pension_code");
						editor = settings.edit();
						editor.putLong("pension_pos", position);
						editor.putString("pension", pension);
						editor.putString("pension_code", pension_code);
						
						editor.commit();
						
						Log.i("Object", "pension " + pension);
						
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
	    	    }
	    	    

	    	    @Override
	    	    public void onNothingSelected(AdapterView<?> parentView) {
	    	        // your code here
	    	    }

	    	});
		   
		}
		
		public static boolean isMedicareValid(String input, boolean validateWithIRN){
		    int[] multipliers = new int[]{1, 3, 7, 9, 1, 3, 7, 9};
		    String pattern = "^(\\d{8})(\\d)";
		    String medicareNumber = input.replace(" " , "");
		    int length = validateWithIRN ? 11 : 10;

		    if (medicareNumber.length() != length) {return false;}

		    Pattern medicatePattern = Pattern.compile(pattern);
		    Matcher matcher = medicatePattern.matcher(medicareNumber);

		    if (matcher.find()){

		        String base = matcher.group(1);
		        String checkDigit = matcher.group(2);

		        int total = 0;
		        for (int i = 0; i < multipliers.length; i++){
		            total += base.charAt(i) * multipliers[i];
		        }

		        return ((total % 10) == Integer.parseInt(checkDigit));
		    }

		    return false;

		}
		
		public boolean validateInput() {
			
			EditText mn = ((EditText) getView().findViewById(R.id.medicare_number));
			String medicare_number = mn.getText().toString();
			
			EditText mln = ((EditText) getView().findViewById(R.id.medicare_line_number));
			String medicare_line_number = mln.getText().toString();
			
			if (medicare_number.length() == 0) {
				return true;
			}
			
			if(!isMedicareValid(medicare_number+medicare_line_number, true)) {
				return false;
			}
			
			return true;
		}

		public void getPensions() {
	    	
	    	Log.i("Getting Pensions", "getting pensions");
	    	Log.i("Getting Pensions URL", "https://secure.docappointment.com.au/apps/patient_details_pensions.php?surgery_id="+surgery_id);
	    	AsyncHttpClient client = new AsyncHttpClient();
	        client.get("https://secure.docappointment.com.au/apps/patient_details_pensions.php?surgery_id="+surgery_id, new AsyncHttpResponseHandler() {
	            @Override
	            public void onSuccess(String response) {
	            	loadPensionsJSONResult(response);
	            }
	        });
	    	
	    }
	 
	    public void loadPensionsJSONResult(String response) {
	    	
	    	editor.putString("pensions_json", response);
			editor.commit();
	    	
	    	ArrayList<HashMap<String, String>> myItems = new ArrayList<HashMap<String, String>>();
	    	
	   	 	JSONObject json = null;
	   	 	
			try {
				json = new JSONObject(response);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}    
	        
	        try{
	        	
	        	pensions_array = json.getJSONArray("pensions");
	        	Log.i("Pensions", "pensions = " + pensions_array);
	        	
	        	String pension;
	        	String pension_code = settings.getString("pension_code", "");
	        	
		        for(int i=0;i<pensions_array.length();i++){						
					HashMap<String, String> map = new HashMap<String, String>();	
					JSONObject e = pensions_array.getJSONObject(i);
					pension = e.getString("pension_type");
					map.put("pension",  pension);
					if (pension_code.equals(e.getString("pension_code"))) {
						final SharedPreferences.Editor editor = settings.edit();
						editor.putLong("pension_pos", i);
						editor.commit();
					}
					map.put("pension_code",  e.getString("pension_code"));
					myItems.add(map);			
				}		
	        }catch(JSONException e)        {
	        	 Log.e("log_tag", "Error parsing data "+e.toString());
	        }
	        
	        SimpleAdapter adapter = new SimpleAdapter(getActivity(), myItems, R.layout.spinner_item,
	                new String[] { "pension" }, 
	                new int[] { R.id.spinner_label_1 });
	      
	        pensionsSpinner.setAdapter(adapter);
	        
	        int pos = (int) settings.getLong("pension_pos", 0);
		    Log.i("Getting Pension Pos", " - " + pos);
		    pensionsSpinner.setSelection(pos, true);
		    
	       //pd.dismiss();
	        
	    }
		public void showDialog() {
			
	    	FragmentTransaction ft = getFragmentManager().beginTransaction(); //get the fragment
	    	
	    	frag = DateDialogFragment.newInstance(getActivity(), new DateDialogFragmentListener(){
	    		public void updateChangedDate(int year, int month, int day){
	    			
	    			String monthString = "";
	    			switch(month) {
	    				case 0:
	    					monthString = "January";
	    					break;
	    				case 1:
	        				monthString = "February";
	        				break;
	    				case 2:
	        				monthString = "March";
	        				break;
	    				case 3:
	        				monthString = "April";
	        				break;
	    				case 4:
	        				monthString = "May";
	        				break;
	    				case 5:
	        				monthString = "June";
	        				break;
	    				case 6:
	        				monthString = "July";
	        				break;
	    				case 7:
	        				monthString = "August";
	        				break;
	    				case 8:
	        				monthString = "September";
	        				break;
	    				case 9:
	        				monthString = "October";
	        				break;
	    				case 10:
	        				monthString = "November";
	        				break;
	    				case 11:
	        				monthString = "Decemeber";
	        				break;
	    			}
	    			button.setText(monthString+" "+String.valueOf(year));
	    			editor.putString("medicare_expiry", String.valueOf(year)+"-"+String.valueOf(month+1)+"-"+String.valueOf(day));
	    			Log.i("medicare_expiry", String.valueOf(year)+"-"+String.valueOf(month+1)+"-"+String.valueOf(day));
	    			editor.commit();
	    			now.set(year, month, day);
	    			Time test_now = new Time();
	    			test_now.setToNow();
	    			if (now.getTimeInMillis() < test_now.toMillis(false)) {
	    				Log.i("medicare_expiry past", now.getTimeInMillis() + " < " + test_now.toMillis(false));
	    				Context context = getActivity();
	    				CharSequence text = "Your Medicare card has expired.\n\nYou need to renew it.";
	    				int duration = Toast.LENGTH_LONG;

	    				Toast toast = Toast.makeText(context, text, duration);
	    				toast.setGravity(Gravity.CENTER|Gravity.CENTER, 0, 0);
	    				ViewGroup group = (ViewGroup) toast.getView();
	    			    TextView messageTextView = (TextView) group.getChildAt(0);
	    			    messageTextView.setTextSize(25);
	    			    messageTextView.setGravity(Gravity.CENTER_HORIZONTAL);
	    				toast.show();
	    			}
	    		}
	    	}, now);
	    	
	    	frag.show(ft, "DateDialogFragment");
	    	
	    }
		
		public void showDialog2() {
			
	    	FragmentTransaction ft = getFragmentManager().beginTransaction(); //get the fragment
	    	
	    	frag = DateDialogFragment.newInstance(getActivity(), new DateDialogFragmentListener(){
	    		public void updateChangedDate(int year, int month, int day){
	    			
	    			String monthString = "";
	    			switch(month) {
	    				case 0:
	    					monthString = "January";
	    					break;
	    				case 1:
	        				monthString = "February";
	        				break;
	    				case 2:
	        				monthString = "March";
	        				break;
	    				case 3:
	        				monthString = "April";
	        				break;
	    				case 4:
	        				monthString = "May";
	        				break;
	    				case 5:
	        				monthString = "June";
	        				break;
	    				case 6:
	        				monthString = "July";
	        				break;
	    				case 7:
	        				monthString = "August";
	        				break;
	    				case 8:
	        				monthString = "September";
	        				break;
	    				case 9:
	        				monthString = "October";
	        				break;
	    				case 10:
	        				monthString = "November";
	        				break;
	    				case 11:
	        				monthString = "Decemeber";
	        				break;
	    			}
	    			button2.setText(String.valueOf(day) + " " + monthString+" "+String.valueOf(year));
	    			editor.putString("pension_expiry", String.valueOf(year)+"-"+String.valueOf(month+1)+"-"+String.valueOf(day));
	    			Log.i("pension_expiry", String.valueOf(year)+"-"+String.valueOf(month+1)+"-"+String.valueOf(day));
	    			editor.commit();
	    			now.set(year, month, day);
	    		}
	    	}, now);
	    	
	    	frag.show(ft, "DateDialogFragment");
	    	
	    }
	    
}
