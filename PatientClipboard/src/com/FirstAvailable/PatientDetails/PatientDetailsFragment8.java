package com.FirstAvailable.PatientDetails;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Spinner;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class PatientDetailsFragment8 extends Fragment {
	
	public JSONArray titles_array;
	public ProgressDialog pd;
	public static final String PREFS_NAME = "MyPrefsFile";
	public static SharedPreferences settings = null;
	public static String surgery_id;
	public Spinner titlesSpinner;
	SharedPreferences.Editor editor;
	
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {
			
	        return inflater.inflate(R.layout.patient_details_8, container, false);
	    }

		@Override
	    public void onStop() {
	    // TODO Auto-generated method stub
	        super.onStop();

	        Log.i("onStop", "stopping step 8");
	        
			EditText fn = ((EditText) getView().findViewById(R.id.nok_first_name));
		    EditText ln = ((EditText) getView().findViewById(R.id.nok_last_name));
		    EditText a = ((EditText) getView().findViewById(R.id.nok_address));
		    EditText c = ((EditText) getView().findViewById(R.id.nok_city));
		    EditText pc = ((EditText) getView().findViewById(R.id.nok_postcode));
		    EditText ph = ((EditText) getView().findViewById(R.id.nok_phone));
		    EditText aph = ((EditText) getView().findViewById(R.id.nok_alternate_phone));
		    EditText r = ((EditText) getView().findViewById(R.id.nok_relationship));
	        
		    String nok_first_name = fn.getText().toString();
			String nok_last_name = ln.getText().toString();
			String nok_address = a.getText().toString();
			String nok_city = c.getText().toString();
			String nok_postcode = pc.getText().toString();
			String nok_phone = ph.getText().toString();
			String nok_alternate_phone = aph.getText().toString();
			String nok_relationship = r.getText().toString();
			
			editor.putString("nok_first_name", nok_first_name);
			editor.putString("nok_last_name", nok_last_name);
			editor.putString("nok_address", nok_address);
			editor.putString("nok_city", nok_city);
			editor.putString("nok_postcode", nok_postcode);
			editor.putString("nok_phone", nok_phone);
			editor.putString("nok_alternate_phone", nok_alternate_phone);
			editor.putString("nok_relationship", nok_relationship);
			
			editor.commit();
			
			Log.i("nok_first_name", " - " + settings.getString("nok_first_name", ""));
			Log.i("nok_last_name", " - " + settings.getString("nok_last_name", ""));
			Log.i("nok_address", " - " + settings.getString("nok_address", ""));
			Log.i("nok_city", " - " + settings.getString("nok_city", ""));
			Log.i("nok_postcode", " - " + settings.getString("nok_postcode", ""));
			Log.i("nok_phone", " - " + settings.getString("nok_phone", ""));
			Log.i("nok_alternate_phone", " - " + settings.getString("nok_alternate_phone", ""));
			Log.i("nok_relationship", " - " + settings.getString("nok_relationship", ""));
			 
		}
		
		@Override
		public void onViewCreated(View view, Bundle savedInstanceState) {
	
		    super.onViewCreated(view, savedInstanceState);
		   
		    settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
		    editor = settings.edit();
		    
		    surgery_id = settings.getString("surgery_id", "");
		    Log.i("surgery_id", surgery_id);
		    
		    titlesSpinner = (Spinner) getView().findViewById(R.id.nok_titles_spinner);
		    
		    String titles_json = settings.getString("titles_json", "");
		    
		    if (titles_json.length() == 0) {
				getTitles();
		    } else {
		    	loadTitlesJSONResult(titles_json);
		    }
		    //Log.i("titles_json", titles_json);
		    
		    titlesSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
	    	    @Override
	    	    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
	    
					try {
						JSONObject selectedTitle = titles_array.getJSONObject(position);
						String title = selectedTitle.getString("title");
						String title_code = selectedTitle.getString("title_code");
						
						editor.putLong("nok_title_pos", position);
						editor.putString("nok_title", title);
						editor.putString("nok_title_code", title_code);
						editor.commit();
						
						//Log.i("Object", "title_code " + title_code);
						
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
	    	    }
	    	    

	    	    @Override
	    	    public void onNothingSelected(AdapterView<?> parentView) {
	    	        // your code here
	    	    }

	    	});
			String nok_first_name = settings.getString("nok_first_name", "");
			String nok_last_name = settings.getString("nok_last_name", "");
			String nok_address = settings.getString("nok_address", "");
			String nok_city = settings.getString("nok_city", "");
			String nok_postcode = settings.getString("nok_postcode", "");
			String nok_phone = settings.getString("nok_phone", "");
			String nok_alternate_phone = settings.getString("nok_alternate_phone", "");
			String nok_relationship = settings.getString("nok_relationship", "");
			
			EditText fn = ((EditText) getView().findViewById(R.id.nok_first_name));
			fn.setText(nok_first_name);
			fn.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
		    EditText ln = ((EditText) getView().findViewById(R.id.nok_last_name));
		    ln.setText(nok_last_name);
		    ln.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
		    EditText a = ((EditText) getView().findViewById(R.id.nok_address));
		    a.setText(nok_address);
		    a.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
		    EditText c = ((EditText) getView().findViewById(R.id.nok_city));
		    c.setText(nok_city);
		    c.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
		    EditText pc = ((EditText) getView().findViewById(R.id.nok_postcode));
		    pc.setText(nok_postcode);
		    pc.setInputType(InputType.TYPE_CLASS_NUMBER);
		    EditText ph = ((EditText) getView().findViewById(R.id.nok_phone));
		    ph.setText(nok_phone);
		    ph.setInputType(InputType.TYPE_CLASS_PHONE);
		    EditText aph = ((EditText) getView().findViewById(R.id.nok_alternate_phone));
		    aph.setText(nok_alternate_phone);
		    aph.setInputType(InputType.TYPE_CLASS_PHONE);
		    EditText r = ((EditText) getView().findViewById(R.id.nok_relationship));
		    r.setText(nok_relationship);
		    r.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
		    
		    //hideKeyboard();
		    
	    }
		
		public void hideKeyboard() {
	    	InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
	    	View view = getActivity().getCurrentFocus();
	        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
	        //View mainView = getView().findViewById(R.id.pdLayout);
	        //mainView.requestFocus();
	    }
		
		public boolean validateInput() {
			
			EditText fn = ((EditText) getView().findViewById(R.id.nok_first_name));
		    EditText ln = ((EditText) getView().findViewById(R.id.nok_last_name));
		    EditText ph = ((EditText) getView().findViewById(R.id.nok_phone));
		    
		    String nok_first_name = fn.getText().toString();
			String nok_last_name = ln.getText().toString();
			String nok_phone = ph.getText().toString();
			
			if(nok_first_name.equals("") || nok_last_name.equals("") || nok_phone.equals("")) {
				return false;
			}
			
			return true;
		}

		public void getTitles() {
	    	
	    	Log.i("Getting Titles", "getting titles");
	    	Log.i("Getting Titles URL", "https://secure.docappointment.com.au/apps/patient_details_titles.php?surgery_id="+surgery_id);
	    	AsyncHttpClient client = new AsyncHttpClient();
	        client.get("https://secure.docappointment.com.au/apps/patient_details_titles.php?surgery_id="+surgery_id, new AsyncHttpResponseHandler() {
	            @Override
	            public void onSuccess(String response) {
	            	loadTitlesJSONResult(response);
	            }
	        });
	    	
	    }
	 
	    public void loadTitlesJSONResult(String response) {
	    	
	    	editor.putString("titles_json", response);
			editor.commit();
			
	    	ArrayList<HashMap<String, String>> myItems = new ArrayList<HashMap<String, String>>();
	    	
	   	 	JSONObject json = null;
	   	 	
			try {
				json = new JSONObject(response);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}    
	        
	        try{
	        	
	        	titles_array = json.getJSONArray("titles");
	        	//Log.i("Titles", "titles = " + titles_array);
	        	
	        	String nok_title_code = settings.getString("nok_title_code", "");
	        	String nok_title = settings.getString("nok_title", "");
	        	String title = "";
	        	
		        for(int i=0;i<titles_array.length();i++){						
					HashMap<String, String> map = new HashMap<String, String>();	
					JSONObject e = titles_array.getJSONObject(i);
					title = e.getString("title");
					map.put("title",  title);
					
					if (nok_title_code.equals(e.getString("title_code")) || nok_title.equals(e.getString("title"))) {
						final SharedPreferences.Editor editor = settings.edit();
						if (nok_title_code.equals(e.getString("title_code")) || nok_title.equals(e.getString("title"))) {
							editor.putLong("nok_title_pos", i);
						} else {
							editor.putLong("nok_title_pos", 0);
						}
						editor.commit();
					}
					map.put("title_code",  e.getString("title_code"));
					myItems.add(map);			
				}		
	        }catch(JSONException e)        {
	        	 Log.e("log_tag", "Error parsing data "+e.toString());
	        }
	        
	        titlesSpinner = (Spinner) getView().findViewById(R.id.nok_titles_spinner);
	        SimpleAdapter adapter = new SimpleAdapter(getActivity(), myItems, R.layout.spinner_item,
	                new String[] { "title" }, 
	                new int[] { R.id.spinner_label_1 });
	      
	        titlesSpinner.setAdapter(adapter);
	        
	        int pos = (int) settings.getLong("nok_title_pos", 0);
		    titlesSpinner.setSelection(pos, true);
		   
	       //pd.dismiss();
	        
	    }
}
