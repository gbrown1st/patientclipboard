package com.FirstAvailable.PatientDetails;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.SimpleAdapter;
import android.widget.Spinner;

public class PatientDetailsFragment4 extends Fragment {
	
	public JSONArray genders_array;
	public JSONArray atsic_array;
	public ProgressDialog pd;
	public static final String PREFS_NAME = "MyPrefsFile";
	public static SharedPreferences settings = null;
	public static String surgery_id;
	public static String dob;
	public Spinner gendersSpinner;
	public static String sms_code;
	public Spinner atsicSpinner;
	public Button smsButton;
	
	
	DateDialogFragment frag;
	Button button;
    Calendar now;
	
	SharedPreferences.Editor editor;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {
			
	        return inflater.inflate(R.layout.patient_details_4, container, false);
	    }
	
	@Override
    public void onStop() {
    // TODO Auto-generated method stub
        super.onStop();

        Log.i("onStop", "stopping step 4");
        editor.putString("sms_code", sms_code);
       
		editor.commit();
		
		Log.i("sms_code", " - " + settings.getString("sms_code", ""));
		 
	}
	
		@Override
		public void onViewCreated(View view, Bundle savedInstanceState) {
	
		    super.onViewCreated(view, savedInstanceState);
		    
		    now = Calendar.getInstance();
		    
		    settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
		    editor = settings.edit();
		    
			dob = settings.getString("dob", "");
			Log.i("dob", dob);
			sms_code = settings.getString("sms_code", "");
			Log.i("sms_code", sms_code);
			surgery_id = settings.getString("surgery_id", "");
			Log.i("surgery_id", surgery_id);
			
			gendersSpinner = (Spinner) getView().findViewById(R.id.gender_spinner);
			atsicSpinner = (Spinner) getView().findViewById(R.id.atsic_spinner);
			smsButton = (Button) getView().findViewById(R.id.btnSMSConsent);
			
			smsButton.setOnClickListener(new View.OnClickListener() {
	        	public void onClick(View v) {
	        		setSMSConsent();	
	        	}
	        });
			
			if (sms_code.equals("1")) {
	    		smsButton.setBackgroundResource(R.drawable.sms_consent_checked);
	    	} else {
	    		smsButton.setBackgroundResource(R.drawable.sms_consent_unchecked);
	    	}
			
			String gender_json = settings.getString("gender_json", "");
		    
		    if (gender_json.length() == 0) {
		    	getGenders();
		    } else {
		    	loadGendersJSONResult(gender_json);
		    }
		    //Log.i("gender_json", gender_json);
		    
		    String[] separated = dob.split("-");
			String month = separated[1];
			
			if (month.equals("01") || month.equals("1")) {
				month = "January";
			} else if (month.equals("02") || month.equals("2")) {
				month = "February";
			} else if (month.equals("03") || month.equals("3")) {
				month = "March";
			} else if (month.equals("04") || month.equals("4")) {
				month = "April";
			} else if (month.equals("05") || month.equals("5")) {
				month = "May";
			} else if (month.equals("06") || month.equals("6")) {
				month = "June";
			} else if (month.equals("07") || month.equals("7")) {
				month = "July";
			} else if (month.equals("08") || month.equals("8")) {
				month = "August";
			} else if (month.equals("09") || month.equals("9")) {
				month = "September";
			} else if (month.equals("10")) {
				month = "October";
			} else if (month.equals("11")) {
				month = "November";
			} else if (month.equals("12")) {
				month = "December";
			}
			
			String dayString = separated[2];
			if (dayString.length() == 2 && dayString.substring(0) == "0") {
				dayString = dayString.substring(1);
			}
			
			hideKeyboard();
			button = (Button) getView().findViewById(R.id.new_dob_btn);
	        button.setText(dayString + " " + month + " " + separated[0]);
	        button.setOnClickListener(new View.OnClickListener() {
	        	public void onClick(View v) {
	        		showDialog();	
	        	}
	        });
	        gendersSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
	    	    @Override
	    	    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
	    
					try {
						JSONObject selectedTitle = genders_array.getJSONObject(position);
						String sex = selectedTitle.getString("sex");
						String gender_code = selectedTitle.getString("sex_code");
						editor = settings.edit();
						editor.putLong("gender_pos", position);
						editor.putString("sex", sex);
						editor.putString("gender_code", gender_code);
						
						editor.commit();
						
						Log.i("Object", "sex " + sex);
						
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
	    	    }
	    	    

	    	    @Override
	    	    public void onNothingSelected(AdapterView<?> parentView) {
	    	        // your code here
	    	    }

	    	});
	        
	        String atsic_json = settings.getString("atsic_json", "");
		    
		    if (atsic_json.length() == 0) {
		    	getATSIC();
		    } else {
		    	loadATSICJSONResult(atsic_json);
		    }
		    Log.i("atsic_json", atsic_json);
		    
		    atsicSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
	    	    @Override
	    	    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
	    
					try {
						JSONObject selectedTitle = atsic_array.getJSONObject(position);
						String atsic = selectedTitle.getString("atsic");
						String atsic_code = selectedTitle.getString("atsic_code");
						if (position == 0) {
							atsic = "";
							atsic_code = "4";
						}
						editor = settings.edit();
						editor.putLong("atsic_pos", position);
						editor.putString("atsic", atsic);
						editor.putString("atsic_code", atsic_code);
						
						editor.commit();
						
						Log.i("Object", "atsic " + atsic);
						Log.i("Object", "atsic_code " + atsic_code);
						
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
	    	    }
	    	    

	    	    @Override
	    	    public void onNothingSelected(AdapterView<?> parentView) {
	    	        // your code here
	    	    }

	    	});
	        
	    }
	
		public void setSMSConsent() {
	    	hideKeyboard();
	    	if (sms_code.equals("")) {
	    		sms_code = "1";
	    		smsButton.setBackgroundResource(R.drawable.sms_consent_checked);
	    	} else {
	    		sms_code = "";
	    		smsButton.setBackgroundResource(R.drawable.sms_consent_unchecked);
	    	}
	    }
		
		public void showDialog() {
			
	    	FragmentTransaction ft = getFragmentManager().beginTransaction(); //get the fragment
	    	
	    	editor = settings.edit();
			
	    	frag = DateDialogFragment.newInstance(getActivity(), new DateDialogFragmentListener(){
	    		public void updateChangedDate(int year, int month, int day){
	    			String monthString = "";
	    			now.set(year, month, day);
	    			Time time = new Time();   
	    			time.setToNow();
	    			if (now.getTimeInMillis() > time.toMillis(true)) {
	    				year = time.year;
	    				month = time.month;
	    				day = time.monthDay;
	    				now.set(year, month, day);
	    			}
	    			
	    			switch(month) {
	    				case 0:
	    					monthString = "January";
	    					break;
	    				case 1:
	        				monthString = "February";
	        				break;
	    				case 2:
	        				monthString = "March";
	        				break;
	    				case 3:
	        				monthString = "April";
	        				break;
	    				case 4:
	        				monthString = "May";
	        				break;
	    				case 5:
	        				monthString = "June";
	        				break;
	    				case 6:
	        				monthString = "July";
	        				break;
	    				case 7:
	        				monthString = "August";
	        				break;
	    				case 8:
	        				monthString = "September";
	        				break;
	    				case 9:
	        				monthString = "October";
	        				break;
	    				case 10:
	        				monthString = "November";
	        				break;
	    				case 11:
	        				monthString = "Decemeber";
	        				break;
	    			}
	    			hideKeyboard();
	    			button.setText(String.valueOf(day)+" "+monthString+" "+String.valueOf(year));
	    			editor.putString("dob", String.valueOf(year)+"-"+String.valueOf(month+1)+"-"+String.valueOf(day));
	    			Log.i("dob", String.valueOf(year)+"-"+String.valueOf(month+1)+"-"+String.valueOf(day));
	    			editor.commit();
	    			
	    		}
	    	}, now);
	    	
	    	frag.show(ft, "DateDialogFragment");
	    	
	    }
	    
	    public interface DateDialogFragmentListener{
	    	//this interface is a listener between the Date Dialog fragment and the activity to update the buttons date
	    	public void updateChangedDate(int year, int month, int day);
	    }
	   
		public void getGenders() {
	    	
	    	Log.i("Getting Genders", "getting genders");
	    	Log.i("Getting Genders URL", "https://secure.docappointment.com.au/apps/patient_details_sex.php?surgery_id="+surgery_id);
	    	AsyncHttpClient client = new AsyncHttpClient();
	        client.get("https://secure.docappointment.com.au/apps/patient_details_sex.php?surgery_id="+surgery_id, new AsyncHttpResponseHandler() {
	            @Override
	            public void onSuccess(String response) {
	            	loadGendersJSONResult(response);
	            }
	        });
	    	
	    }
	 
		public void hideKeyboard() {
	    	InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
	    	View view = getActivity().getCurrentFocus();
	        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
	        //View mainView = getView().findViewById(R.id.pdLayout);
	        //mainView.requestFocus();
	    }
		
		public void loadGendersJSONResult(String response) {
	    	
			editor.putString("gender_json", response);
			editor.commit();
			
	    	ArrayList<HashMap<String, String>> myItems = new ArrayList<HashMap<String, String>>();
	    	
	   	 	JSONObject json = null;
	   	 	
			try {
				json = new JSONObject(response);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}    
	        
	        try{
	        	
	        	genders_array = json.getJSONArray("genders");
	        	Log.i("Genders", "genders = " + genders_array);
	        	
	        	String sex;
	        	String sex_first_char = "";
	        	char sex_initial;
	        	String gender_code = settings.getString("gender_code", "");
	        	
		        for(int i=0;i<genders_array.length();i++){						
					HashMap<String, String> map = new HashMap<String, String>();	
					JSONObject e = genders_array.getJSONObject(i);
					sex = e.getString("sex");
					if (sex.length() > 0) {
						sex_initial = sex.charAt(0);
						sex_first_char = Character.toString(sex_initial);
					} 
					Log.i("sex_first_char", "sex_first_char = " + sex_first_char);
					map.put("sex",  sex);
					
					if (gender_code.equals(e.getString("sex_code")) || gender_code.equals(sex_first_char)) {
						final SharedPreferences.Editor editor = settings.edit();
						editor.putLong("gender_pos", i);
						editor.commit();
					}
					
					map.put("gender_code",  e.getString("sex_code"));
					myItems.add(map);			
				}		
	        }catch(JSONException e)        {
	        	 Log.e("log_tag", "Error parsing data "+e.toString());
	        }
	        
	        Spinner gendersSpinner = (Spinner) getView().findViewById(R.id.gender_spinner);
	        SimpleAdapter adapter = new SimpleAdapter(getActivity(), myItems, R.layout.spinner_item,
	                new String[] { "sex" }, 
	                new int[] { R.id.spinner_label_1 });
	      
	        gendersSpinner.setAdapter(adapter);
	        
	        int pos = (int) settings.getLong("gender_pos", 0);
		    Log.i("Getting Gender Pos", " - " + pos);
		    gendersSpinner.setSelection(pos, true);
		   
	       //pd.dismiss();
	        
	    }
		
		public void getATSIC() {
	    	
	    	Log.i("Getting ATSIC", "getting atsic");
	    	Log.i("Getting ATSIC URL", "https://secure.docappointment.com.au/apps/patient_details_atsic.php?surgery_id="+surgery_id);
	    	AsyncHttpClient client = new AsyncHttpClient();
	        client.get("https://secure.docappointment.com.au/apps/patient_details_atsic.php?surgery_id="+surgery_id, new AsyncHttpResponseHandler() {
	            @Override
	            public void onSuccess(String response) {
	            	loadATSICJSONResult(response);
	            }
	        });
	    	
	    }
	 
	   
		public void loadATSICJSONResult(String response) {
	    	
			editor.putString("atsic_json", response);
			editor.commit();
			
	    	ArrayList<HashMap<String, String>> myItems = new ArrayList<HashMap<String, String>>();
	    	
	   	 	JSONObject json = null;
	   	 	
			try {
				json = new JSONObject(response);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}    
	        
	        try{
	        	
	        	atsic_array = json.getJSONArray("atsic");
	        	Log.i("ATSIC", "atsic = " + atsic_array);
	        	
	        	String atsic_code = settings.getString("atsic_code", "");
	        	String atsic = settings.getString("atsic", "");
	        	
		        for(int i=0;i<atsic_array.length();i++){						
		        	HashMap<String, String> map = new HashMap<String, String>();	
					JSONObject e = atsic_array.getJSONObject(i);
					atsic = e.getString("atsic");
					map.put("atsic",  atsic);
					
					if (atsic_code.equals(e.getString("atsic_code")) || atsic_code.equals(e.getString("atsic"))) {
						final SharedPreferences.Editor editor = settings.edit();
						editor.putLong("atsic_pos", i);
						editor.commit();
					}
					map.put("atsic_code",  e.getString("atsic_code"));
					myItems.add(map);	
				}		
	        }catch(JSONException e)        {
	        	 Log.e("log_tag", "Error parsing data "+e.toString());
	        }
	        
	        Spinner atsicSpinner = (Spinner) getView().findViewById(R.id.atsic_spinner);
	        SimpleAdapter adapter = new SimpleAdapter(getActivity(), myItems, R.layout.spinner_item,
	                new String[] { "atsic" }, 
	                new int[] { R.id.spinner_label_1 });
	      
	        atsicSpinner.setAdapter(adapter);
	        
	        int pos = (int) settings.getLong("atsic_pos", 0);
		    Log.i("Getting ATSIC Pos", " - " + pos);
		    atsicSpinner.setSelection(pos, true);
		   
	       //pd.dismiss();
	        
	    }
}
