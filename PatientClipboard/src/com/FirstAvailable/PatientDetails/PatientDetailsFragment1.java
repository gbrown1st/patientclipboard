package com.FirstAvailable.PatientDetails;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Spinner;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class PatientDetailsFragment1 extends Fragment {
	
	public JSONArray titles_array;
	public ProgressDialog pd;
	public static final String PREFS_NAME = "MyPrefsFile";
	public static SharedPreferences settings = null;
	public static String surgery_id;
	public Spinner titlesSpinner;
	SharedPreferences.Editor editor;
	
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {
			
	        return inflater.inflate(R.layout.patient_details_1, container, false);
	    }

		@Override
	    public void onStop() {
	    // TODO Auto-generated method stub
	        super.onStop();

	        Log.i("onStop", "stopping step 1");
	        
			EditText fn = ((EditText) getView().findViewById(R.id.first_name));
		    EditText mn = ((EditText) getView().findViewById(R.id.middle_name));
		    EditText ln = ((EditText) getView().findViewById(R.id.last_name));
		    EditText pn = ((EditText) getView().findViewById(R.id.preferred_name));
	        
		    String first_name = fn.getText().toString();
			String middle_name = mn.getText().toString();
			String last_name = ln.getText().toString();
			String preferred_name = pn.getText().toString();
			
			editor.putString("first_name", first_name);
			editor.putString("middle_name", middle_name);
			editor.putString("last_name", last_name);
			editor.putString("preferred_name", preferred_name);
			
			editor.commit();
			
			Log.i("first_name", " - " + settings.getString("first_name", ""));
			Log.i("middle_name", " - " + settings.getString("middle_name", ""));
			Log.i("last_name", " - " + settings.getString("last_name", ""));
			Log.i("preferred_name", " - " + settings.getString("preferred_name", ""));
			 
		}
		
		@Override
		public void onViewCreated(View view, Bundle savedInstanceState) {
	
		    super.onViewCreated(view, savedInstanceState);
		   
		    settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
		    editor = settings.edit();
		    
		    surgery_id = settings.getString("surgery_id", "");
		    Log.i("surgery_id", surgery_id);
		    
		    titlesSpinner = (Spinner) getView().findViewById(R.id.titles_spinner);
		    
		    String titles_json = settings.getString("titles_json", "");
		    
		    if (titles_json.length() == 0) {
				getTitles();
		    } else {
		    	loadTitlesJSONResult(titles_json);
		    }
		    //Log.i("titles_json", titles_json);
		    
		    titlesSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
	    	    @Override
	    	    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
	    
					try {
						JSONObject selectedTitle = titles_array.getJSONObject(position);
						String title = selectedTitle.getString("title");
						String title_code = selectedTitle.getString("title_code");
						
						editor.putLong("title_pos", position);
						editor.putString("title", title);
						editor.putString("title_code", title_code);
						editor.commit();
						
						//Log.i("Object", "title_code " + title_code);
						
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
	    	    }
	    	    

	    	    @Override
	    	    public void onNothingSelected(AdapterView<?> parentView) {
	    	        // your code here
	    	    }

	    	});
		    
			String first_name = settings.getString("first_name", "");
			String middle_name = settings.getString("middle_name", "");
			String last_name = settings.getString("last_name", "");
			String preferred_name = settings.getString("preferred_name", "");
			
			EditText fn = ((EditText) getView().findViewById(R.id.first_name));
			fn.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
		    fn.setText(first_name);
		    EditText mn = ((EditText) getView().findViewById(R.id.middle_name));
		    mn.setText(middle_name);
		    mn.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
		    EditText ln = ((EditText) getView().findViewById(R.id.last_name));
		    ln.setText(last_name);
		    ln.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
		    EditText pn = ((EditText) getView().findViewById(R.id.preferred_name));
		    pn.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
		    pn.setText(preferred_name);
		   
		    //hideKeyboard();
		    
	    }
		
		public void hideKeyboard() {
	    	InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
	    	View view = getActivity().getCurrentFocus();
	        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
	        //View mainView = getView().findViewById(R.id.pdLayout);
	        //mainView.requestFocus();
	    }
		
		public boolean validateInput() {
			
			EditText fn = ((EditText) getView().findViewById(R.id.first_name));
		    EditText ln = ((EditText) getView().findViewById(R.id.last_name));
	        
		    String first_name = fn.getText().toString();
			String last_name = ln.getText().toString();
			
			if(first_name.equals("") || last_name.equals("")) {
				return false;
			}
			
			return true;
		}

		public void getTitles() {
	    	
	    	Log.i("Getting Titles", "getting titles");
	    	Log.i("Getting Titles URL", "https://secure.docappointment.com.au/apps/patient_details_titles.php?surgery_id="+surgery_id);
	    	AsyncHttpClient client = new AsyncHttpClient();
	        client.get("https://secure.docappointment.com.au/apps/patient_details_titles.php?surgery_id="+surgery_id, new AsyncHttpResponseHandler() {
	            @Override
	            public void onSuccess(String response) {
	            	loadTitlesJSONResult(response);
	            }
	        });
	    	
	    }
	 
	    public void loadTitlesJSONResult(String response) {
	    	
	    	editor.putString("titles_json", response);
			editor.commit();
			
	    	ArrayList<HashMap<String, String>> myItems = new ArrayList<HashMap<String, String>>();
	    	
	   	 	JSONObject json = null;
	   	 	
			try {
				json = new JSONObject(response);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}    
	        
	        try{
	        	
	        	titles_array = json.getJSONArray("titles");
	        	//Log.i("Titles", "titles = " + titles_array);
	        	
	        	String title_code = settings.getString("title_code", "");
	        	String title;
	        	
		        for(int i=0;i<titles_array.length();i++){						
					HashMap<String, String> map = new HashMap<String, String>();	
					JSONObject e = titles_array.getJSONObject(i);
					title = e.getString("title");
					map.put("title",  title);
					
					if (title_code.equals(e.getString("title_code")) || title_code.equals(e.getString("title"))) {
						final SharedPreferences.Editor editor = settings.edit();
						editor.putLong("title_pos", i);
						editor.commit();
					}
					map.put("title_code",  e.getString("title_code"));
					myItems.add(map);			
				}		
	        }catch(JSONException e)        {
	        	 Log.e("log_tag", "Error parsing data "+e.toString());
	        }
	        
	        titlesSpinner = (Spinner) getView().findViewById(R.id.titles_spinner);
	        SimpleAdapter adapter = new SimpleAdapter(getActivity(), myItems, R.layout.spinner_item,
	                new String[] { "title" }, 
	                new int[] { R.id.spinner_label_1 });
	      
	        titlesSpinner.setAdapter(adapter);
	        
	        int pos = (int) settings.getLong("title_pos", 0);
		    //Log.i("Getting Titles Pos", " - " + pos);
		    titlesSpinner.setSelection(pos, true);
		   
	       //pd.dismiss();
	        
	    }
}
