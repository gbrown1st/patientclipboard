package com.FirstAvailable.PatientDetails;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class NewPatientActivity extends Activity implements OnClickListener {
	
	public static final String PREFS_NAME = "MyPrefsFile";
	public static SharedPreferences settings = null;
	public static String current_fragment = "Frag 1";
	public static String previous_fragment = "";
	public static String next_fragment = "Frag 2";
	public ProgressDialog pd;
	private AlertDialog alert;
	private Boolean isCancelling = false;
	SharedPreferences.Editor editor;
	
	public static String logo;
	public static String logo_landscape;
	public static String surgery_id;
	
	Bitmap b;
    ImageView practiceLogo;
    
    Bitmap l;
    ImageView practiceLogoLandscape;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.patient_details);
		
		Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(this.getResources().getColor(R.color.black));
        
        practiceLogo = (ImageView)findViewById(R.id.practiceLogo);
        settings = getSharedPreferences(PREFS_NAME, 0);
        logo = settings.getString("logo", "");
        logo_landscape = settings.getString("logo_landscape", "");
        surgery_id = settings.getString("surgery_id", "");
	    Log.i("surgery_id", surgery_id);
        
        information info = new information();
        info.execute("");
        
		View newPatientStepOneBtnClick = findViewById(R.id.pd_back_btn);
		newPatientStepOneBtnClick.setOnClickListener(this);
		
		View newPatientStepTwoBtnClick = findViewById(R.id.pd_next_btn);
		newPatientStepTwoBtnClick.setOnClickListener(this);
		
		View newPatientSaveBtnClick = findViewById(R.id.pd_save_btn);
		newPatientSaveBtnClick.setOnClickListener(this);
		
		View newPatientCancelBtnClick = findViewById(R.id.pd_cancel_btn);
		newPatientCancelBtnClick.setOnClickListener(this);
		
		View prog1BtnClick = findViewById(R.id.btnProg1);
		prog1BtnClick.setOnClickListener(this);
		
		View prog2BtnClick = findViewById(R.id.btnProg2);
		prog2BtnClick.setOnClickListener(this);
		
		View prog3BtnClick = findViewById(R.id.btnProg3);
		prog3BtnClick.setOnClickListener(this);
		
		View prog4BtnClick = findViewById(R.id.btnProg4);
		prog4BtnClick.setOnClickListener(this);
		
		View prog5BtnClick = findViewById(R.id.btnProg5);
		prog5BtnClick.setOnClickListener(this);
		
		View prog6BtnClick = findViewById(R.id.btnProg6);
		prog6BtnClick.setOnClickListener(this);
		
		View prog7BtnClick = findViewById(R.id.btnProg7);
		prog7BtnClick.setOnClickListener(this);
		
		View prog8BtnClick = findViewById(R.id.btnProg8);
		prog8BtnClick.setOnClickListener(this);
		
		View prog9BtnClick = findViewById(R.id.btnProg9);
		prog9BtnClick.setOnClickListener(this);
		
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		ft.replace(R.id.fragment_placeholder, new PatientDetailsFragment1());
		ft.commit(); 
		
		current_fragment = "Frag 1";
		previous_fragment = "";
		next_fragment = "Frag 2";
		
		setButtons();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
	    if (event.getAction() == MotionEvent.ACTION_DOWN) {
	        View v = getCurrentFocus();
	        if ( v instanceof EditText) {
	            Rect outRect = new Rect();
	            v.getGlobalVisibleRect(outRect);
	            if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
	                v.clearFocus();
	                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
	                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
	            }
	        }
	    }
	    return super.dispatchTouchEvent( event );
	}
	
	public void addPatient() throws UnsupportedEncodingException {
    	
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        String patient_id = settings.getString("patient_id", "");
        String surgery_id = settings.getString("surgery_id", "");
        String patient_details_checked = settings.getString("patient_details_checked", "");
        String title = URLEncoder.encode(settings.getString("title", ""), "UTF-8");
        String title_code = URLEncoder.encode(settings.getString("title_code", ""), "UTF-8");
        String first_name =  URLEncoder.encode(settings.getString("first_name", ""), "UTF-8");
		String middle_name =  URLEncoder.encode(settings.getString("middle_name", ""), "UTF-8");
		String last_name =  URLEncoder.encode(settings.getString("last_name", ""), "UTF-8");
		String preferred_name =  URLEncoder.encode(settings.getString("preferred_name", ""), "UTF-8");
		String address_1 =  URLEncoder.encode(settings.getString("address_1", ""), "UTF-8");
		String address_2 =  URLEncoder.encode(settings.getString("address_2", ""), "UTF-8");
		String city =  URLEncoder.encode(settings.getString("city", ""), "UTF-8");
		String postcode =  URLEncoder.encode(settings.getString("postcode", ""), "UTF-8");
		String postal_address =  URLEncoder.encode(settings.getString("postal_address", ""), "UTF-8");
		String postal_city =  URLEncoder.encode(settings.getString("postal_city", ""), "UTF-8");
		String postal_postcode =  URLEncoder.encode(settings.getString("postal_postcode", ""), "UTF-8");
		
		String card =  URLEncoder.encode(settings.getString("card", ""), "UTF-8");
		String allergies =  URLEncoder.encode(settings.getString("allergies", ""), "UTF-8");
		String medications =  URLEncoder.encode(settings.getString("medications", ""), "UTF-8");
		String medical_conditions = settings.getString("medical_conditions", "");
		String family_history =  URLEncoder.encode(settings.getString("family_history", ""), "UTF-8");
		String non_smoker =  URLEncoder.encode(settings.getString("non_smoker", ""), "UTF-8");
		String smoker =  URLEncoder.encode(settings.getString("smoker", ""), "UTF-8");
		String smokes_per_day =  URLEncoder.encode(settings.getString("smokes_per_day", ""), "UTF-8");
		String ex_smoker =  URLEncoder.encode(settings.getString("ex_smoker", ""), "UTF-8");
		String hear =  URLEncoder.encode(settings.getString("hear", ""), "UTF-8");
		String hear_other =  URLEncoder.encode(settings.getString("hear_other", ""), "UTF-8");
		
		String home_phone =  URLEncoder.encode(settings.getString("home_phone", ""), "UTF-8");
		String work_phone =  URLEncoder.encode(settings.getString("work_phone", ""), "UTF-8");
		String mobile_phone =  URLEncoder.encode(settings.getString("mobile_phone", ""), "UTF-8");
		String email =  URLEncoder.encode(settings.getString("email", ""), "UTF-8");
		String dob =  URLEncoder.encode(settings.getString("dob", ""), "UTF-8");
		String gender =  URLEncoder.encode(settings.getString("sex", ""), "UTF-8");
		String gender_code =  URLEncoder.encode(settings.getString("gender_code", ""), "UTF-8");
		String atsic_code =  URLEncoder.encode(settings.getString("atsic_code", ""), "UTF-8");
		String sms_code =  URLEncoder.encode(settings.getString("sms_code", ""), "UTF-8");
		String medicare_number =  URLEncoder.encode(settings.getString("medicare_number", ""), "UTF-8");
		String medicare_line_number =  URLEncoder.encode(settings.getString("medicare_line_number", ""), "UTF-8");
		String medicare_expiry =  URLEncoder.encode(settings.getString("medicare_expiry", ""), "UTF-8");
		String pension_code =  URLEncoder.encode(settings.getString("pension_code", ""), "UTF-8");
		String dva_code =  URLEncoder.encode(settings.getString("dva_code", ""), "UTF-8");
		String pension_number =  URLEncoder.encode(settings.getString("pension_number", ""), "UTF-8");
		String pension_expiry =  URLEncoder.encode(settings.getString("pension_expiry", ""), "UTF-8");
		String dva_number =  URLEncoder.encode(settings.getString("dva_number", ""), "UTF-8");
		String head_of_family =  URLEncoder.encode(settings.getString("head_of_family", ""), "UTF-8");
		String nok_title = URLEncoder.encode(settings.getString("nok_title", ""), "UTF-8");
        String nok_title_code = URLEncoder.encode(settings.getString("nok_title_code", ""), "UTF-8");
		String nok_first_name = URLEncoder.encode(settings.getString("nok_first_name", ""), "UTF-8");
		String nok_last_name = URLEncoder.encode(settings.getString("nok_last_name", ""), "UTF-8");
		String nok_address = URLEncoder.encode(settings.getString("nok_address", ""), "UTF-8");
		String nok_city = URLEncoder.encode(settings.getString("nok_city", ""), "UTF-8");
		String nok_postcode = URLEncoder.encode(settings.getString("nok_postcode", ""), "UTF-8");
		String nok_phone = URLEncoder.encode(settings.getString("nok_phone", ""), "UTF-8");
		String nok_alternate_phone = URLEncoder.encode(settings.getString("nok_alternate_phone", ""), "UTF-8");
		String nok_relationship = URLEncoder.encode(settings.getString("nok_relationship", ""), "UTF-8");
		String ec_title = URLEncoder.encode(settings.getString("ec_title", ""), "UTF-8");
        String ec_title_code = URLEncoder.encode(settings.getString("ec_title_code", ""), "UTF-8");
		String ec_first_name = URLEncoder.encode(settings.getString("ec_first_name", ""), "UTF-8");
		String ec_last_name = URLEncoder.encode(settings.getString("ec_last_name", ""), "UTF-8");
		String ec_address = URLEncoder.encode(settings.getString("ec_address", ""), "UTF-8");
		String ec_city = URLEncoder.encode(settings.getString("ec_city", ""), "UTF-8");
		String ec_postcode = URLEncoder.encode(settings.getString("ec_postcode", ""), "UTF-8");
		String ec_phone = URLEncoder.encode(settings.getString("ec_phone", ""), "UTF-8");
		String ec_alternate_phone = URLEncoder.encode(settings.getString("ec_alternate_phone", ""), "UTF-8");
		String ec_relationship = URLEncoder.encode(settings.getString("ec_relationship", ""), "UTF-8");
		
		String use_nok_as_ec = settings.getString("use_nok_as_ec", "");
		
		if (use_nok_as_ec.equals("1")) {
			ec_title = nok_title;
			ec_title_code = nok_title_code;
			ec_first_name = nok_first_name;
			ec_last_name = nok_last_name;
			ec_address = nok_address;
			ec_city = nok_city;
			ec_postcode = nok_postcode;
			ec_phone = nok_phone;
			ec_alternate_phone = nok_alternate_phone;
			ec_relationship = nok_relationship;
		} 
		
		String params = "";
		
		Log.i("patient_details_checked", patient_details_checked);
		
		if (patient_details_checked.equals("no")) {
			
			 goToComplete();
			 
		} else {
			
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			
			if (!atsic_code.equals("4") && surgery_id.equals("142")) {
				
				String message = "Please register the patient manually and ensure that an Indigenous Health Incentive form has been signed.";
				
		    	builder.setTitle("Indigenous Patient")
					   .setMessage(message)
				       .setCancelable(false)
				       .setNegativeButton("OK", new DialogInterface.OnClickListener() {
				           public void onClick(DialogInterface dialog, int id) {
				        	   dialog.cancel();
							   return;
				           }
				       });
				
			} else {
				
				builder.create();
		  		alert = builder.create();
		  		alert.show();
				
				pd = new ProgressDialog(this);
		        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		        pd.setMessage("Adding the new patient details...for "+patient_id);
		        pd.show();
		        
				String url;
				AsyncHttpClient client = new AsyncHttpClient();
				
				if (patient_id == "") {
					
					params = "surgery_id="+surgery_id+"&title="+title+"&title_code="+title_code+"&first_name="+first_name;
					params += "&middle_name="+middle_name+"&last_name="+last_name+"&preferred_name="+preferred_name+"&address_1="+address_1+"&address_2="+address_2;
					params += "&city="+city+"&postcode="+postcode+"&postal_address="+postal_address+"&postal_city="+postal_city+"&postal_postcode="+postal_postcode;
					
					params += "&card="+card+"&allergies="+allergies+"&medications="+medications+"&medical_conditions="+medical_conditions+"&family_history="+family_history+"&smoker="+smoker;
					params += "&smokes_per_day="+smokes_per_day+"&non_smoker="+non_smoker+"&ex_smoker="+ex_smoker+"&hear="+hear+"&hear_other="+hear_other;

					params += "&home_phone="+home_phone+"&work_phone="+work_phone+"&mobile_phone="+mobile_phone+"&email="+email+"&dob="+dob+"&gender="+gender+"&gender_code="+gender_code+"&atsic_code="+atsic_code+"&sms_code="+sms_code;
					params += "&medicare_number="+medicare_number+"&medicare_line_number="+medicare_line_number+"&medicare_expiry="+medicare_expiry;
					params += "&pension_code="+pension_code+"&dva_code="+dva_code+"&pension_number="+pension_number+"&pension_expiry="+pension_expiry+"&dva_number="+dva_number;
					params += "&nok_title="+nok_title+"&nok_title_code="+nok_title_code+"&nok_first_name="+nok_first_name+"&nok_last_name="+nok_last_name+"&nok_address="+nok_address+"&nok_city="+nok_city+"&nok_postcode="+nok_postcode+"&nok_phone="+nok_phone+"&nok_alternate_phone="+nok_alternate_phone+"&nok_relationship="+nok_relationship;
					params += "&ec_title="+ec_title+"&ec_title_code="+ec_title_code+"&ec_first_name="+ec_first_name+"&ec_last_name="+ec_last_name+"&ec_address="+ec_address+"&ec_city="+ec_city+"&ec_postcode="+ec_postcode+"&ec_phone="+ec_phone+"&ec_alternate_phone="+ec_alternate_phone+"&ec_relationship="+ec_relationship;
					params = params.replace(" ", "+");
					url = "https://secure.docappointment.com.au/apps/new_patient_details.php?"+params;
					
				} else {
					
					params = "surgery_id="+surgery_id+"&patient_id="+patient_id+"&title="+title+"&title_code="+title_code+"&first_name="+first_name;
					params += "&middle_name="+middle_name+"&last_name="+last_name+"&preferred_name="+preferred_name+"&address_1="+address_1+"&address_2="+address_2;
					params += "&city="+city+"&postcode="+postcode+"&postal_address="+postal_address+"&postal_city="+postal_city+"&postal_postcode="+postal_postcode;
					params += "&home_phone="+home_phone+"&work_phone="+work_phone+"&mobile_phone="+mobile_phone+"&email="+email+"&dob="+dob+"&gender="+gender+"&gender_code="+gender_code+"&atsic_code="+atsic_code+"&sms_code="+sms_code;
					params += "&medicare_number="+medicare_number+"&medicare_line_number="+medicare_line_number+"&medicare_expiry="+medicare_expiry;
					params += "&pension_code="+pension_code+"&dva_code="+dva_code+"&pension_number="+pension_number+"&pension_expiry="+pension_expiry+"&dva_number="+dva_number+"&head_of_family="+head_of_family;
					params += "&nok_title="+nok_title+"&nok_title_code="+nok_title_code+"&nok_first_name="+nok_first_name+"&nok_last_name="+nok_last_name+"&nok_address="+nok_address+"&nok_city="+nok_city+"&nok_postcode="+nok_postcode+"&nok_phone="+nok_phone+"&nok_alternate_phone="+nok_alternate_phone+"&nok_relationship="+nok_relationship;
					params += "&ec_title="+ec_title+"&ec_title_code="+ec_title_code+"&ec_first_name="+ec_first_name+"&ec_last_name="+ec_last_name+"&ec_address="+ec_address+"&ec_city="+ec_city+"&ec_postcode="+ec_postcode+"&ec_phone="+ec_phone+"&ec_alternate_phone="+ec_alternate_phone+"&ec_relationship="+ec_relationship;
					params = params.replace(" ", "+");
					url = "https://secure.docappointment.com.au/apps/existing_patient_details.php?"+params;
					
				}
				
		    	Log.i("url", url);
		    	pd.dismiss();
		    	if (!url.equals("")) {
		    		client.get(url, new AsyncHttpResponseHandler() {
		                @Override
		                public void onSuccess(String response) {
		                	addPatientJSONResult(response);
		                }
		            });
		    	}
				
				
			}
			
		}
	
    }
    
	public void goToComplete() {
		settings = getSharedPreferences(PREFS_NAME, 0);
		String surgery_code = settings.getString("surgery_code", "");
		surgery_code = surgery_code.trim();
		
		if (surgery_code.isEmpty()) {
			try {
				editor = settings.edit();
				editor.putString("patient_details_checked", "yes");
				editor.commit();
				addPatient();
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			Intent cd = new Intent(this, CompleteDetailsActivity.class);  
	     	startActivity(cd);
		}
		
	}
	
	public void returnToHome() {
		Intent np = new Intent(this, MainActivity.class);  
     	startActivity(np);
	}
	
    public void addPatientJSONResult(String response) {
    	
    	pd.dismiss();
    	
    	JSONObject json = null;
   	 	
		try {
			json = new JSONObject(response);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}    
        
        try{
        
        	JSONArray result = json.getJSONArray("result");
        	JSONObject e = result.getJSONObject(0);
        	Log.i("Save survey responses result", result.toString());
        	
        	AlertDialog.Builder builder = new AlertDialog.Builder(this);
        	
        	if (e.getString("heading").equals("failed")) {
        		builder.setTitle(e.getString("heading"))
   			   .setMessage(e.getString("message"))
   		       .setCancelable(false)
   		       .setNegativeButton("OK", new DialogInterface.OnClickListener() {
   		           public void onClick(DialogInterface dialog, int id) {
   		        	goToComplete();
   		           }
   		       });
        	} else {
        		builder.setTitle(e.getString("heading"))
   			   .setMessage(e.getString("message"))
   		       .setCancelable(false)
   		       .setNegativeButton("OK", new DialogInterface.OnClickListener() {
   		           public void onClick(DialogInterface dialog, int id) {
   		        	 returnToHome();
   		           }
   		       });
        	}
      		builder.create();
      		alert = builder.create();
      		alert.show();
			
      		
        }catch(JSONException e)        {
        	 Log.e("log_tag", "Error parsing data "+e.toString());
        }
    	
    }
	
	public void setButtons() {
		
		Log.i("setButtons - current_fragment", current_fragment);
		Button btn_1 = (Button) findViewById(R.id.pd_back_btn);
		Button btn_2 = (Button) findViewById(R.id.pd_next_btn);
		Button btn_save = (Button) findViewById(R.id.pd_save_btn);
		btn_save.setEnabled(true);
		ImageView progress = (ImageView) findViewById(R.id.progressImage);
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        String patient_id = settings.getString("patient_id", "");
        
    		ImageView heroIcon = (ImageView) findViewById(R.id.heroImage);
    		TextView landTitle = (TextView) findViewById(R.id.landTitle);
    		if (patient_id == "") {
    			heroIcon.setImageResource(R.drawable.hero_new_patient);
	    		landTitle.setText(R.string.new_patient_title);
			} else {
				heroIcon.setImageResource(R.drawable.hero_existing_patient);
        		landTitle.setText(R.string.existing_patient_title);
			}
    
		if (current_fragment.equals("Frag 1")) {
			progress.setImageResource(R.drawable.progress_1);
				if (patient_id == "") {
    				
    			} else {
    				//bgView.setBackgroundResource(R.drawable.existing_patient_1);
    			}
				if (surgery_id.equals("142")) {
					btn_save.setEnabled(false);
				}
			btn_1.setEnabled(false);
			btn_2.setEnabled(true);
		} else if (current_fragment.equals("Frag 9")) {
			progress.setImageResource(R.drawable.progress_9);
				if (patient_id == "") {
    				//bgView.setBackgroundResource(R.drawable.patient_7);
    			} else {
    				//bgView.setBackgroundResource(R.drawable.existing_patient_7);
    			}
			btn_1.setEnabled(true);
			btn_2.setEnabled(true);
		} else {
			if (current_fragment.equals("Frag 2")) {
				progress.setImageResource(R.drawable.progress_2);
				if (surgery_id.equals("142")) {
					btn_save.setEnabled(false);
				}
					if (patient_id == "") {
	    				
	    			} else {
	    				//bgView.setBackgroundResource(R.drawable.existing_patient_2);
	    			}
			} else if (current_fragment.equals("Frag 3")) {
				progress.setImageResource(R.drawable.progress_3);
				if (surgery_id.equals("142")) {
					btn_save.setEnabled(false);
				}
				if (patient_id == "") {

    			} else {
    				//bgView.setBackgroundResource(R.drawable.existing_patient_3);
    			}
			} else if (current_fragment.equals("Frag 4")) {
				progress.setImageResource(R.drawable.progress_4);
				if (surgery_id.equals("142")) {
					btn_save.setEnabled(false);
				}
				if (patient_id == "") {

    			} else {
    				//bgView.setBackgroundResource(R.drawable.existing_patient_4);
    			}
			} else if (current_fragment.equals("Frag 5")) {
				progress.setImageResource(R.drawable.progress_5);
				if (surgery_id.equals("142")) {
					btn_save.setEnabled(false);
				}
				if (patient_id == "") {

    			} else {
    				//bgView.setBackgroundResource(R.drawable.existing_patient_5);
    			}
			} else if (current_fragment.equals("Frag 6")) {
				progress.setImageResource(R.drawable.progress_6);
				if (patient_id == "") {

    			} else {
    				//bgView.setBackgroundResource(R.drawable.existing_patient_6);
    			}
			} else if (current_fragment.equals("Frag 7")) {
				progress.setImageResource(R.drawable.progress_7);
				if (patient_id == "") {

    			} else {
    				//bgView.setBackgroundResource(R.drawable.existing_patient_6);
    			}
			} else if (current_fragment.equals("Frag 8")) {
				progress.setImageResource(R.drawable.progress_8);
				if (patient_id == "") {

    			} else {
    				//bgView.setBackgroundResource(R.drawable.existing_patient_6);
    			}
			} else if (current_fragment.equals("Frag 9")) {
				progress.setImageResource(R.drawable.progress_9);
				if (patient_id == "") {

    			} else {
    				//bgView.setBackgroundResource(R.drawable.existing_patient_6);
    			}
			}
			btn_1.setEnabled(true);
			btn_2.setEnabled(true);
		}
	}
	
	public void inputValidationAlert(String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setTitle("Missing Information")
			   .setMessage(message)
		       .setCancelable(false)
		       .setNegativeButton("OK", new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		        	dialog.cancel();
		           }
		       });
    	
  		builder.create();
  		alert = builder.create();
  		alert.show();
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

	    if (keyCode == KeyEvent.KEYCODE_BACK ) {
	    	 return false;
	    }

	    return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public void onClick(View v) {
		
		Log.i("btn view", v.toString());
		
		if (v.getId() == R.id.pd_cancel_btn) {
			isCancelling = true;
		} else {
			isCancelling = false;
		}
		
		Fragment currentFragment  = getFragmentManager().findFragmentById(R.id.fragment_placeholder);
		
		if (current_fragment.equals("Frag 1") && !isCancelling && (currentFragment instanceof PatientDetailsFragment1)) {
			
			PatientDetailsFragment1 theFrag = (PatientDetailsFragment1) getFragmentManager().findFragmentById(R.id.fragment_placeholder);
			if (theFrag.validateInput()){
				previous_fragment = "";
				next_fragment = "Frag 2";
			} else {
				String message = "Please make sure you have entered your first name and last name.";
				inputValidationAlert(message);
	      		return;
			}
			
		} else if (current_fragment.equals("Frag 2") && !isCancelling && (currentFragment instanceof PatientDetailsFragment2)) {
			PatientDetailsFragment2 theFrag = (PatientDetailsFragment2) getFragmentManager().findFragmentById(R.id.fragment_placeholder);
			if (theFrag.validateInput()){
				previous_fragment = "Frag 1";
				next_fragment = "Frag 3";
			} else {
				String message = "Please make sure you have entered a valid postcode.";
				inputValidationAlert(message);
	      		return;
			}
			
		} else if (current_fragment.equals("Frag 3") && ((currentFragment instanceof PatientDetailsFragment3) || (currentFragment instanceof PatientDetailsFragment10))) {
	        previous_fragment = "Frag 2";
			next_fragment = "Frag 4";
			if (currentFragment instanceof PatientDetailsFragment3) {
				PatientDetailsFragment3 theFrag = (PatientDetailsFragment3) getFragmentManager().findFragmentById(R.id.fragment_placeholder);
				if (!theFrag.validateInput()){
					String message = "Please make sure you have entered a valid postcode.";
					inputValidationAlert(message);
		      		return;
				}
			}
	        
		} else if (current_fragment.equals("Frag 4") && (currentFragment instanceof PatientDetailsFragment4)) {
			previous_fragment = "Frag 3";
			next_fragment = "Frag 5";
		} else if (current_fragment.equals("Frag 5") && !isCancelling && (currentFragment instanceof PatientDetailsFragment5)) {
			
			PatientDetailsFragment5 theFrag = (PatientDetailsFragment5) getFragmentManager().findFragmentById(R.id.fragment_placeholder);
			if (theFrag.validateInput()){
				previous_fragment = "Frag 4";
				next_fragment = "Frag 6";
			} else {
				String message = "If you are entering any landline numbers (home, work) please use only numbers without spaces.\n\nLandline numbers should be between 8 and 12 digits long.\n\nMobile numbers should begin with 04 and be ten digits long.\n\nIf you are entering an email address please make sure it's valid.";
				inputValidationAlert(message);
	      		return;
			}
			
		} else if (current_fragment.equals("Frag 6") && !isCancelling && (currentFragment instanceof PatientDetailsFragment6)) {
			/*
			PatientDetailsFragment6 theFrag = (PatientDetailsFragment6) getFragmentManager().findFragmentById(R.id.fragment_placeholder);
			
			if (theFrag.validateInput()){
				previous_fragment = "Frag 5";
				next_fragment = "Frag 7";
			} else {
				String message = "Please make sure you have entered a valid medicare number and medicare line number and that you have entered the medicare expiry.";
				inputValidationAlert(message);
	      		return;
			}
			*/
			previous_fragment = "Frag 5";
			next_fragment = "Frag 7";
		} else if (current_fragment.equals("Frag 7") && (currentFragment instanceof PatientDetailsFragment7)) {
			
			previous_fragment = "Frag 6";
			next_fragment = "Frag 8";
			
		} else if (current_fragment.equals("Frag 8") && (currentFragment instanceof PatientDetailsFragment8)) {
			previous_fragment = "Frag 7";
			next_fragment = "Frag 9";
			
		} else if (current_fragment.equals("Frag 9") && (currentFragment instanceof PatientDetailsFragment9)) {
			previous_fragment = "Frag 8";
			next_fragment = "";
		} 
		
		// TODO Auto-generated method stub
		
		switch(v.getId()){
		
		
		case R.id.btnProg1:
			
			previous_fragment = current_fragment;
			current_fragment = "Frag 1";
			next_fragment = "Frag 2";
			setButtons();
			break;
			
		case R.id.btnProg2:
			
			previous_fragment = current_fragment;
			current_fragment = "Frag 2";
			next_fragment = "Frag 3";
			setButtons();
			break;
			
		case R.id.btnProg3:
	
			previous_fragment = current_fragment;
			current_fragment = "Frag 3";
			next_fragment = "Frag 4";
			setButtons();
			break;
			
		case R.id.btnProg4:
			
			previous_fragment = current_fragment;
			current_fragment = "Frag 4";
			next_fragment = "Frag 5";
			setButtons();
			break;
			
		case R.id.btnProg5:
			
			previous_fragment = current_fragment;
			current_fragment = "Frag 5";
			next_fragment = "Frag 6";
			setButtons();
			break;
			
		case R.id.btnProg6:
			
			previous_fragment = current_fragment;
			current_fragment = "Frag 6";
			next_fragment = "Frag 7";
			setButtons();
			break;
			
		case R.id.btnProg7:
			
			previous_fragment = current_fragment;
			current_fragment = "Frag 7";
			next_fragment = "";
			setButtons();
			break;
			
		case R.id.btnProg8:
			
			previous_fragment = current_fragment;
			current_fragment = "Frag 8";
			next_fragment = "";
			setButtons();
			break;
			
		case R.id.btnProg9:
	
			previous_fragment = current_fragment;
			current_fragment = "Frag 9";
			next_fragment = "";
			setButtons();
			break;
			
		case R.id.pd_back_btn:
			Log.i("pd_back_btn", "sent click");
			if (previous_fragment.equals("")) {
				return;
			}
			
			current_fragment = previous_fragment;
			setButtons();
			
			
			break;
			
		case R.id.pd_next_btn:
			
			if (current_fragment.equals("Frag 9")) {
				try {
					addPatient();
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (next_fragment.equals("")) {
				return;
			}
			
			current_fragment = next_fragment;
			setButtons();
			break;
			
		case R.id.pd_save_btn:
			
			try {
					addPatient();
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			break;
			
		
		case R.id.pd_cancel_btn:
			
			returnToHome();
			break;
		
		}
		
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		
		if (current_fragment.equals("Frag 1")) {
			ft.replace(R.id.fragment_placeholder, new PatientDetailsFragment1());
		} else if (current_fragment.equals("Frag 2")) {
			ft.replace(R.id.fragment_placeholder, new PatientDetailsFragment2());
		} else if (current_fragment.equals("Frag 3")) {
			SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
	        String use_pracsoft_alt_screen = settings.getString("use_pracsoft_alt_screen", "");
	        if (use_pracsoft_alt_screen.equals("y")) {
	        	ft.replace(R.id.fragment_placeholder, new PatientDetailsFragment10());
	        } else {
	        	ft.replace(R.id.fragment_placeholder, new PatientDetailsFragment3());
	        }
		} else if (current_fragment.equals("Frag 4")) {
			ft.replace(R.id.fragment_placeholder, new PatientDetailsFragment4());
		} else if (current_fragment.equals("Frag 5")) {
			ft.replace(R.id.fragment_placeholder, new PatientDetailsFragment5());
		} else if (current_fragment.equals("Frag 6")) {
			ft.replace(R.id.fragment_placeholder, new PatientDetailsFragment6());
		} else if (current_fragment.equals("Frag 7")) {
			ft.replace(R.id.fragment_placeholder, new PatientDetailsFragment7());
		} else if (current_fragment.equals("Frag 8")) {
			ft.replace(R.id.fragment_placeholder, new PatientDetailsFragment8());
		} else if (current_fragment.equals("Frag 9")) {
			ft.replace(R.id.fragment_placeholder, new PatientDetailsFragment9());
		}
		ft.addToBackStack("TAG");
		ft.commit();
	}
	
	public class information extends AsyncTask<String, String, String>
    {
        @Override
        protected String doInBackground(String... arg0) {

            try
            {
                URL url = new URL(logo);
                InputStream is = new BufferedInputStream(url.openStream());
                b = BitmapFactory.decodeStream(is);
                
                URL urlLandscape = new URL(logo_landscape);
                InputStream isl = new BufferedInputStream(urlLandscape.openStream());
                l = BitmapFactory.decodeStream(isl);

            } catch(Exception e){}
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
        	
        	String rotation = getRotation(NewPatientActivity.this);
        	
        	if (rotation.equals("landscape")) {
        		practiceLogo.setImageBitmap(b);
        	} else {
        		practiceLogo.setImageBitmap(l);
        	}
        	
        }
    }
	
	public String getRotation(Context context){
	
	    final int rotation = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getRotation();
	           switch (rotation) {
	            case Surface.ROTATION_0:
	                return "portrait";
	            case Surface.ROTATION_90:
	                return "landscape";
	            case Surface.ROTATION_180:
	                return "portrait";
	            default:
	                return "portrait";
	            }
	        }
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		Log.i("onConfigurationChanged", "onConfigurationChanged" );
	    super.onConfigurationChanged(newConfig);  
	    information info = new information();
        info.execute("");
	}
	
}

