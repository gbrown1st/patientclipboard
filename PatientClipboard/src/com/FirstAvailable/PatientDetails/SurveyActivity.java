package com.FirstAvailable.PatientDetails;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class SurveyActivity extends Activity implements OnClickListener {
	
	public static final String PREFS_NAME = "MyPrefsFile";
	public static SharedPreferences settings = null;
	public static String current_fragment = "Frag 1";
	public ProgressDialog pd;
	public static String survey_title;
	public static String survey_id;
	public static String surgery_id;
	public JSONArray survey_questions_array;
	public int number_of_questions;
	public int current_question;
	public static String question_type;
	public static ArrayList<HashMap<String, String>> myItems;
	private AlertDialog alert;
	public TextView survey_progress;
	public Handler textViewHandler;
	public static String progess;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		current_question = 0;
		myItems = new ArrayList<HashMap<String, String>>();
		
		setContentView(R.layout.survey);
		settings = getSharedPreferences(PREFS_NAME, 0);
		
		survey_title = settings.getString("survey_title", "");
		survey_id = settings.getString("survey_id", "");
		surgery_id = settings.getString("surgery_id", "");
		
		survey_progress = (TextView) findViewById(R.id.survey_progress);
		textViewHandler = new Handler();
		
		TextView survey_heading_tv = (TextView) findViewById(R.id.survey_heading);
		survey_heading_tv.setText(survey_title);
		getSurveyQuestions();
		
		View surveyBackBtnClick = findViewById(R.id.survey_back_btn);
		surveyBackBtnClick.setOnClickListener(this);
		
		View surveyNextBtnClick = findViewById(R.id.survey_next_btn);
		surveyNextBtnClick.setOnClickListener(this);
		
		View surveyCancelBtnClick = findViewById(R.id.survey_cancel_btn);
		surveyCancelBtnClick.setOnClickListener(this);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public boolean saveResponse() {
		
		String set_survey_answers = settings.getString("survey_answers", "");
		
		final SharedPreferences.Editor editor = settings.edit();
		
		String survey_question_id = myItems.get(current_question).get("survey_question_id");
		String survey_answers = survey_question_id + "*";
		
		if (question_type.equals("Free text")) {
			EditText ft = ((EditText) findViewById(R.id.free_text_answer));
			String response = ft.getText().toString();
			survey_answers += response + "|";
			set_survey_answers += survey_answers;
			Log.i("survey_answers", " - " + set_survey_answers);
			editor.putString("survey_answers", set_survey_answers);
			editor.commit();
	     } else if (question_type.equals("5 point scale")) {
	    	RatingBar rb = ((RatingBar) findViewById(R.id.ratingBar1));
	    	int response = (int) rb.getRating();
	    	survey_answers += response + "|";
	    	set_survey_answers += survey_answers;
	    	Log.i("survey_answers", " - " + set_survey_answers);
			editor.putString("survey_answers", set_survey_answers);
			editor.commit();
	     }
		return true;
	}
	
	public void setBackQuestion() {
		
		if (current_question > 0) {
			
			current_question --;
			String survey_question_id = myItems.get(current_question).get("survey_question_id");
			String saved_survey_answers = settings.getString("survey_answers", "");
			String regex_string = "(?="+survey_question_id+")(.*)(?=|)"; 
			if (saved_survey_answers.indexOf("|") > -1) {
				saved_survey_answers = saved_survey_answers.replaceAll(regex_string, "");
			}
			
			Log.i("saved_survey_answers", " - " + saved_survey_answers);
			final SharedPreferences.Editor editor = settings.edit();
			editor.putString("survey_answers", saved_survey_answers);
			editor.commit();
			
			TextView survey_question = (TextView) findViewById(R.id.survey_question);
	        question_type = myItems.get(current_question).get("question_type");
	        survey_question.setText(myItems.get(current_question).get("question_text"));
	        showAnswerType();
	        
		}
		
	}

	public void setNextQuestion() {
		
		if (saveResponse()) {
		
			current_question ++;
			
			if (current_question <= number_of_questions - 1) {
				TextView survey_question = (TextView) findViewById(R.id.survey_question);
		        question_type = myItems.get(current_question).get("question_type");
		        survey_question.setText(myItems.get(current_question).get("question_text"));
		        showAnswerType();
		        
			} else {
				
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle("Survey Finshed");
				builder.setMessage("To save your reponses to this survey click OK. To delete your reponses to this survey press cancel.");
	
				builder.setPositiveButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								saveSurveyResponses();
							}
						});
	
				builder.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
					
							public void onClick(DialogInterface dialog,
									int whichButton) {
								return;
							}
						});
	
				alert = builder.create();
				alert.show();
			}
		
		}
		
	}
	
	 public void saveSurveyResponses() {
	    	
		 String survey_patient_id = settings.getString("survey_patient_id", "");
		 String survey_answers = settings.getString("survey_answers", "");
		 String query = null;
		 try {
			query = URLEncoder.encode(survey_answers, "utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
	    	Log.i("Saving Survey Responses", "-");
	    	AsyncHttpClient client = new AsyncHttpClient();
	    	Log.i("Saving Responses URL", "https://secure.docappointment.com.au/apps/save_survey_questions.php?patient_id="+survey_patient_id+"+&survey_answers="+query);
	        client.get("https://secure.docappointment.com.au/apps/save_survey_questions.php?patient_id="+survey_patient_id+"+&survey_answers="+query, new AsyncHttpResponseHandler() {
	            @Override
	            public void onSuccess(String response) {
	            	loadsaveSurveyResponsesJSONResult(response);
	            }
	        });
	    	
	    }
	 
	    public void loadsaveSurveyResponsesJSONResult(String response) {
	    	
	   	 	JSONObject json = null;
	   	 	
			try {
				json = new JSONObject(response);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}    
	        
	        try{
	        	
	        	JSONArray result = json.getJSONArray("result");
	        	JSONObject e = result.getJSONObject(0);
	        	Log.i("Save surver responses result", result.toString());
	        	
	        	AlertDialog.Builder builder = new AlertDialog.Builder(this);
	      		builder.setTitle(e.getString("heading"))
	      			   .setMessage(e.getString("message"))
	      		       .setCancelable(false)
	      		       .setNegativeButton("OK", new DialogInterface.OnClickListener() {
	      		           public void onClick(DialogInterface dialog, int id) {
	      		        	 closeSurvey();
	      		           }
	      		       });
	      		builder.create();
	      		alert = builder.create();
	      		alert.show();
				
	        }catch(JSONException e)        {
	        	 Log.e("log_tag", "Error parsing data "+e.toString());
	        }
	        
	       //pd.dismiss();
	        
	    }
	
	public void closeSurvey() {
		Intent np = new Intent(this, MainActivity.class);  
     	 startActivity(np);
	}
	public void getSurveyQuestions() {
    	
    	Log.i("Getting Survey Questions", "Getting Survey Questions");
    	Log.i("Getting Survey Questions URL", "https://secure.docappointment.com.au/apps/patient_survey_questions.php?surgery_id="+surgery_id+"&survey_id="+survey_id);
    	AsyncHttpClient client = new AsyncHttpClient();
        client.get("https://secure.docappointment.com.au/apps/patient_survey_questions.php?surgery_id="+surgery_id+"&survey_id="+survey_id, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String response) {
            	loadSurveyQuestionsJSONResult(response);
            }
        });
    	
    }
 
	// runnable to allow updating the UI from the thread
	Runnable updateTextView = new Runnable() {
		
	    public void run() {
	    	survey_progress.setText(progess);
	    	Log.i("progess", " - " + progess);
	    }
	};

	public void showAnswerType() {
		question_type = myItems.get(current_question).get("question_type");
		progess = "Question " + (current_question + 1) + " of " + number_of_questions;
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		if (question_type.equals("Free text")) {
		   	ft.replace(R.id.survey_fragment_placeholder, new SurveyAnswerFragment1());
	     } else if (question_type.equals("5 point scale")) {
	    	ft.replace(R.id.survey_fragment_placeholder, new SurveyAnswerFragment2());
	     }
		ft.commit(); 
		textViewHandler.post(updateTextView);
		
	}
	
    public void loadSurveyQuestionsJSONResult(String response) {
    	
   	 	JSONObject json = null;
   	 	
		try {
			json = new JSONObject(response);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}    
        
        try{
        	
        	survey_questions_array = json.getJSONArray("survey_questions");
        	Log.i("survey_questions", "survey_questions = " + survey_questions_array);
        	number_of_questions = survey_questions_array.length();
	        for(int i=0;i<number_of_questions;i++){						
				HashMap<String, String> map = new HashMap<String, String>();	
				JSONObject e = survey_questions_array.getJSONObject(i);
				map.put("survey_question_id",  e.getString("survey_question_id"));
				map.put("question_text",  e.getString("question_text"));
				map.put("question_type",  e.getString("question_type"));
				myItems.add(map);			
			}		
	       
        }catch(JSONException e)        {
        	 Log.e("log_tag", "Error parsing data "+e.toString());
        }
        Log.i("myItems", myItems.get(current_question).get("question_type"));
        TextView survey_question = (TextView) findViewById(R.id.survey_question);
        question_type = myItems.get(current_question).get("question_type");
        survey_question.setText(myItems.get(current_question).get("question_text"));
        showAnswerType();
       //pd.dismiss();
        
    }

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		
		case R.id.survey_back_btn:
			setBackQuestion();
			break;
			
		case R.id.survey_next_btn:
			setNextQuestion();
			break;
			
		case R.id.survey_cancel_btn:
			Intent np = new Intent(this, MainActivity.class);  
	     	startActivity(np);
			break;
			
		}
		
	}

}
