package com.FirstAvailable.PatientDetails;


import java.util.regex.Pattern;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

public class PatientDetailsFragment5 extends Fragment {
	
	public static final String PREFS_NAME = "MyPrefsFile";
	public static SharedPreferences settings = null;
	
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {
	        return inflater.inflate(R.layout.patient_details_5, container, false);
	    }
		
		@Override
	    public void onStop() {
	    // TODO Auto-generated method stub
	        super.onStop();

	        Log.i("onStop", "stopping step 1");
	        
	        settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
	        final SharedPreferences.Editor editor = settings.edit();
	        
	        EditText hp = ((EditText) getView().findViewById(R.id.home_phone));
		    EditText wp = ((EditText) getView().findViewById(R.id.work_phone));
		    EditText mp = ((EditText) getView().findViewById(R.id.mobile_phone));
		    EditText ea = ((EditText) getView().findViewById(R.id.email));
	        
		    String home_phone = hp.getText().toString();
			String work_phone = wp.getText().toString();
			String mobile_phone = mp.getText().toString();
			String email = ea.getText().toString();
			
			editor.putString("home_phone", home_phone);
			editor.putString("work_phone", work_phone);
			editor.putString("mobile_phone", mobile_phone);
			editor.putString("email", email);
			
			editor.commit();
			
			Log.i("home_phone", " - " + settings.getString("home_phone", ""));
			Log.i("work_phone", " - " + settings.getString("work_phone", ""));
			Log.i("mobile_phone", " - " + settings.getString("mobile_phone", ""));
			Log.i("email", " - " + settings.getString("email", ""));
			 
		}

		@Override
		public void onViewCreated(View view, Bundle savedInstanceState) {
	
		    super.onViewCreated(view, savedInstanceState);
		    
		    settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
			
			String home_phone = settings.getString("home_phone", "");
			String work_phone = settings.getString("work_phone", "");
			String mobile_phone = settings.getString("mobile_phone", "");
			String email = settings.getString("email", "");
			
			EditText hp = ((EditText) getView().findViewById(R.id.home_phone));
			hp.setText(home_phone);
		    EditText wp = ((EditText) getView().findViewById(R.id.work_phone));
		    wp.setText(work_phone);
		    EditText mp = ((EditText) getView().findViewById(R.id.mobile_phone));
		    mp.setText(mobile_phone);
		    EditText ea = ((EditText) getView().findViewById(R.id.email));
		    ea.setText(email);
		    
	    }
		
		public boolean validateInput() {
			
			EditText hp = ((EditText) getView().findViewById(R.id.home_phone));
			EditText wp = ((EditText) getView().findViewById(R.id.work_phone));
			EditText mp = ((EditText) getView().findViewById(R.id.mobile_phone));
			EditText ea = ((EditText) getView().findViewById(R.id.email));
			
			String home_phone = hp.getText().toString();
			String work_phone = wp.getText().toString();
			String mobile_phone = mp.getText().toString();
			String email = ea.getText().toString();
			String landPhoneRegex = "^[0-9]{8,12}$";
			String mobilePhoneRegex = "^(04)[0-9]{8}$";
			
			if(!home_phone.matches(landPhoneRegex) && home_phone.length() > 0) {
				return false;
			}
			
			if(!work_phone.matches(landPhoneRegex) && work_phone.length() > 0) {
				return false;
			}
			
			if(!mobile_phone.matches(mobilePhoneRegex) && mobile_phone.length() > 0) {
				return false;
			}
			
			if(!mobile_phone.matches(mobilePhoneRegex) && mobile_phone.length() > 0) {
				return false;
			}
			
			if(!isValidEmail(email) && email.length() > 0) {
				return false;
			}
			
			return true;
		}
		
		public boolean isValidEmail(String email) {
			Pattern pattern = Patterns.EMAIL_ADDRESS;
		    return pattern.matcher(email).matches();

		}
}
