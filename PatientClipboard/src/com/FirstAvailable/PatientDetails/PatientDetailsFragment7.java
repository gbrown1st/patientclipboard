package com.FirstAvailable.PatientDetails;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Spinner;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class PatientDetailsFragment7 extends Fragment {
	
	public JSONArray dva_array;
	public ProgressDialog pd;
	public static final String PREFS_NAME = "MyPrefsFile";
	public static SharedPreferences settings = null;
	public static String surgery_id;
	public Spinner dvaSpinner;
	
	DateDialogFragment frag;
	Button button;
    Calendar now;
    Context thiscontext;
    
    SharedPreferences.Editor editor;
	
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {
			
	        return inflater.inflate(R.layout.patient_details_7, container, false);
	    }
		
		@Override
	    public void onStop() {
	    // TODO Auto-generated method stub
	        super.onStop();

	        Log.i("onStop", "stopping step 7");
	        
		    EditText dva = ((EditText) getView().findViewById(R.id.dva_number));
		    
			String dva_number = dva.getText().toString();
			
			editor.putString("dva_number", dva_number);
			
			editor.commit();
			
			Log.i("dva_number", " - " + settings.getString("dva_number", ""));
			 
		}

		@Override
		public void onViewCreated(View view, Bundle savedInstanceState) {
	
		    super.onViewCreated(view, savedInstanceState);
		    
		    settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
	        editor = settings.edit();
	        
			surgery_id = settings.getString("surgery_id", "");
			
			dvaSpinner = (Spinner) getView().findViewById(R.id.dva_spinner);
			
			String dva_json = settings.getString("dva_json", "");
			Log.i("dva_json", dva_json);
			
			if (dva_json.length() == 0) {
				getDVA();
		    } else {
		    	loadDVAJSONResult(dva_json);
		    }
			
			String dva_number = settings.getString("dva_number", "");
		    EditText dva = ((EditText) getView().findViewById(R.id.dva_number));
		    dva.setText(dva_number);
		    
		    dvaSpinner = (Spinner) getView().findViewById(R.id.dva_spinner);
		    dvaSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
	    	    @Override
	    	    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
	    
					try {
						JSONObject selectedTitle = dva_array.getJSONObject(position);
						String dva_type = selectedTitle.getString("dva_type");
						String dva_code = selectedTitle.getString("dva_code");
						editor = settings.edit();
						editor.putLong("dva_pos", position);
						editor.putString("dva_type", dva_type);
						editor.putString("dva_code", dva_code);
						
						editor.commit();
						
						Log.i("Object", "dva_code " + dva_code);
						
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
	    	    }
	    	    

	    	    @Override
	    	    public void onNothingSelected(AdapterView<?> parentView) {
	    	        // your code here
	    	    }

	    	});
		    
	    }
		
		
	    
	    public void getDVA() {
	    	
	    	Log.i("Getting DVA", "getting dva");
	    	Log.i("Getting DVA URL", "https://secure.docappointment.com.au/apps/patient_details_dva.php?surgery_id="+surgery_id);
	    	AsyncHttpClient client = new AsyncHttpClient();
	        client.get("https://secure.docappointment.com.au/apps/patient_details_dva.php?surgery_id="+surgery_id, new AsyncHttpResponseHandler() {
	            @Override
	            public void onSuccess(String response) {
	            	loadDVAJSONResult(response);
	            }
	        });
	    	
	    }
	 
	    public void loadDVAJSONResult(String response) {
	    	
	    	editor.putString("dva_json", response);
			editor.commit();
	    	
	    	ArrayList<HashMap<String, String>> myItems = new ArrayList<HashMap<String, String>>();
	    	
	   	 	JSONObject json = null;
	   	 	
			try {
				json = new JSONObject(response);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}    
	        
	        try{
	        	
	        	dva_array = json.getJSONArray("dva");
	        	Log.i("DVA", "dva = " + dva_array);
	        	
	        	String dva_type;
	        	String dva_code = settings.getString("dva_code", "");
	        	
		        for(int i=0;i<dva_array.length();i++){						
					HashMap<String, String> map = new HashMap<String, String>();	
					JSONObject e = dva_array.getJSONObject(i);
					dva_type = e.getString("dva_type");
					map.put("dva_type",  dva_type);
					if (dva_code.equals(e.getString("dva_code"))) {
						final SharedPreferences.Editor editor = settings.edit();
						editor.putLong("dva_pos", i);
						editor.commit();
					}
					map.put("dva_code",  e.getString("dva_code"));
					myItems.add(map);			
				}		
	        }catch(JSONException e)        {
	        	 Log.e("log_tag", "Error parsing data "+e.toString());
	        }
	        
	        SimpleAdapter adapter = new SimpleAdapter(getActivity(), myItems, R.layout.spinner_item_2,
	                new String[] { "dva_type" }, 
	                new int[] { R.id.surveys_spinner_label });
	      
	        dvaSpinner.setAdapter(adapter);
	        
	        int pos = (int) settings.getLong("dva_pos", 0);
		    Log.i("Getting dva_pos Pos", " - " + pos);
		    dvaSpinner.setSelection(pos, true);
		   
	       //pd.dismiss();
	        
	    }
}
