package com.FirstAvailable.PatientDetails;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.Surface;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class MainActivity extends Activity  implements OnClickListener {

	public JSONArray surgery_status_array;
	public JSONArray patient_details_array;
	public ProgressDialog pd;
	private AlertDialog alert;
	public static final String PREFS_NAME = "MyPrefsFile";
	public static SharedPreferences settings = null;
	public static String surgery_id;
	public static String patient_id;
	public static String logo;
	public static String logo_landscape;
	
	Calendar now;
	
	Bitmap b;
    ImageView practiceLogo;
    
    Bitmap l;
    ImageView practiceLogoLandscape;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(this.getResources().getColor(R.color.black));
        
        practiceLogo = (ImageView)findViewById(R.id.practiceLogo);
		
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		
		settings = getSharedPreferences(PREFS_NAME, 0);
	    surgery_id = settings.getString("surgery_id", "");
	    Log.i("surgery_id", surgery_id);
		
		checkStatus();
		
		View newPatientBtnClick = findViewById(R.id.new_patient_btn);
		newPatientBtnClick.setOnClickListener(this);
		
		View existingPatientBtnClick = findViewById(R.id.existing_patient_btn);
		if (surgery_id.equals("142")) {
			existingPatientBtnClick.setVisibility(View.GONE);
		}
		
		existingPatientBtnClick.setOnClickListener(this);
		
		View surveysBtnClick = findViewById(R.id.survey_btn);
		surveysBtnClick.setVisibility(View.GONE);
		surveysBtnClick.setOnClickListener(this);
		
		setBlankPatient();
        
	}

	public void setBlankPatient() {
		
		final SharedPreferences.Editor editor = settings.edit();

		editor.putString("patient_details_checked", "no");
		editor.putString("survey_patient_id", "");
		editor.putString("patient_id", "");
		editor.putLong("title_pos", 0);
		editor.putString("title_code", "");
		editor.putString("first_name", "");
		editor.putString("middle_name", "");
		editor.putString("last_name", "");
		editor.putString("preferred_name", "");
		editor.putString("address_1", "");
		editor.putString("address_2", "");
		editor.putString("city", "");
		
		editor.putString("card", "");
		editor.putString("allergies", "");
		editor.putString("medications", "");
		editor.putString("medical_conditions", "");
		editor.putString("family_history", "");
		editor.putString("non_smoker", "");
		editor.putString("ex_smoker", "");
		editor.putString("smoker", "");
		editor.putString("smokes_per_day", "");
		editor.putString("hear", "");
		editor.putString("hear_other", "");
		
		editor.putString("postcode", "");
		editor.putString("postal_address", "");
		editor.putString("postal_city", "");
		editor.putString("postal_postcode", "");
		editor.putString("home_phone", "");
		editor.putString("work_phone", "");
		editor.putString("mobile_phone", "");
		editor.putString("email", "");
		editor.putLong("gender_pos", 0);
		editor.putString("gender_code", "");
		editor.putLong("atsic_pos", 0);
		editor.putString("atsic_code", "");
		editor.putString("sms_code", "1");
		editor.putString("home_phone", "");
		editor.putString("work_phone", "");
		editor.putString("mobile_phone", "");
		editor.putString("email", "");
		editor.putString("medicare_number", "");
		editor.putString("medicare_line_number", "");
		editor.putLong("pension_pos", 0);
		editor.putString("pension_code", "");
		editor.putLong("dva_pos", 0);
		editor.putString("dva_code", "");
		editor.putString("pension_number", "");
		editor.putString("dva_number", "");
		editor.putLong("nok_title_pos", 0);
		editor.putString("nok_title_code", "");
		editor.putString("nok_title", "");
		editor.putString("nok_first_name", "");
		editor.putString("nok_first_name", "");
		editor.putString("nok_last_name", "");
		editor.putString("nok_address", "");
		editor.putString("nok_city", "");
		editor.putString("nok_postcode", "");
		editor.putString("nok_phone", "");
		editor.putString("nok_alternate_phone", "");
		editor.putString("nok_relationship", "");
		editor.putLong("ec_title_pos", 0);
		editor.putString("ec_title_code", "");
		editor.putString("ec_title", "");
		editor.putString("ec_first_name", "");
		editor.putString("ec_first_name", "");
		editor.putString("ec_last_name", "");
		editor.putString("ec_address", "");
		editor.putString("ec_city", "");
		editor.putString("ec_postcode", "");
		editor.putString("ec_phone", "");
		editor.putString("ec_alternate_phone", "");
		editor.putString("ec_relationship", "");
		editor.putString("use_nok_as_ec", "");
		
	    now = Calendar.getInstance();
        //int y = now.get(Calendar.YEAR);
        //int m = now.get(Calendar.MONTH)+1;
        //int d = now.get(Calendar.DAY_OF_MONTH);
		/*
        editor.putString("dob", y + "-" + m +"-" + d);
        editor.putString("medicare_expiry", y + "-" + m +"-" + d);
        editor.putString("pension_expiry", y + "-" + m +"-" + d);
        */
        editor.putString("dob", " -  - ");
        editor.putString("dob_display", "");
        editor.putString("medicare_expiry",  " -  - ");
        editor.putString("medicare_expiry_display", "");
        editor.putString("pension_expiry",  " -  - ");
        editor.putString("pension_expiry_display", "");
        editor.commit();
        
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void startSurvey() {
		Intent bs = new Intent(this, StartSurveyActivity.class);  
		startActivity(bs);
	}
	 public void setSurveyPatientID() {
	    	
	    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    	final SharedPreferences.Editor editor = settings.edit();
	    	editor.putString("survey_answers", "");
			editor.commit();
			builder.setTitle("Anonymous Survey?");
			builder.setMessage("If this survey is not anonymous please enter the patient's ID number and click OK. If the survey is anonymous press cancel.");

			// Set an EditText view to get user input
			final EditText input = new EditText(this);
			input.setLines(1);
			input.setInputType(InputType.TYPE_CLASS_TEXT);
			
			input.setImeActionLabel("Custom text", KeyEvent.KEYCODE_ENTER);

			builder.setView(input);

			builder.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int whichButton) {
							Editable value = input.getText();
							editor.putString("survey_patient_id", value.toString());
							editor.commit();
							startSurvey();
						}
					});

			builder.setNegativeButton("Cancel",
					new DialogInterface.OnClickListener() {
				
						public void onClick(DialogInterface dialog,
								int whichButton) {
							editor.putString("survey_patient_id", "");
							editor.commit();
							startSurvey();
						}
					});

			alert = builder.create();
			alert.show();
		
 }
	 
	 public void getExistingPatient() {
	    	
		    	AlertDialog.Builder builder = new AlertDialog.Builder(this);

				builder.setTitle("Enter Patient ID");
				builder.setMessage("Please enter the patient's ID number.");

				// Set an EditText view to get user input
				final EditText input = new EditText(this);
				input.setLines(1);
				input.setInputType(InputType.TYPE_CLASS_TEXT);
				
				input.setImeActionLabel("Custom text", KeyEvent.KEYCODE_ENTER);

				builder.setView(input);

				builder.setPositiveButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								Editable value = input.getText();
								patient_id = value.toString();
								getExistingPatientDetails();
							}
						});

				builder.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
					
							public void onClick(DialogInterface dialog,
									int whichButton) {
								dialog.cancel();
								return;
							}
						});

				alert = builder.create();
				alert.show();
			
	    }
	 
	 public void getExistingPatientDetails() {
	    	
	    	Log.i("Getting Existing Patient Details", "getting details");
	    	Log.i("Getting Existing Patient Details URL", "https://secure.docappointment.com.au/apps/patient_details.php?surgery_id="+surgery_id+"&patient_id="+patient_id);
	    	AsyncHttpClient client = new AsyncHttpClient();
	        client.get("https://secure.docappointment.com.au/apps/patient_details.php?surgery_id="+surgery_id+"&patient_id="+patient_id, new AsyncHttpResponseHandler() {
	            @Override
	            public void onSuccess(String response) {
	            	loadPatientDetailsJSONResult(response);
	            }
	        });
	    	
	    }
	 
	    public void loadPatientDetailsJSONResult(String response) {
	    	
	   	 	JSONObject json = null;
	   	 	
			try {
				json = new JSONObject(response);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}    
	        
	        try{
	        	
	        	patient_details_array = json.getJSONArray("patient_details");
	        	Log.i("Patient Details", "Patient Details = " + patient_details_array);
	        	settings = getSharedPreferences(PREFS_NAME, 0);
	        	final SharedPreferences.Editor editor = settings.edit();
	        		
		        for(int i=0;i<patient_details_array.length();i++){					
		        	
					JSONObject e = patient_details_array.getJSONObject(i);
					
					editor.putString("patient_id", e.getString("INTERNALID"));
					editor.putLong("title_pos", 0);
					editor.putString("title_code", e.getString("TITLECODE"));
					editor.putString("first_name", e.getString("FIRSTNAME"));
					editor.putString("middle_name", e.getString("MIDDLENAME"));
					editor.putString("last_name", e.getString("SURNAME"));
					editor.putString("preferred_name", e.getString("PREFERREDNAME"));
					editor.putString("address_1", e.getString("ADDRESS1"));
					editor.putString("address_2", e.getString("ADDRESS2"));
					editor.putString("city", e.getString("CITY"));
					editor.putString("postcode", e.getString("POSTCODE"));
					editor.putString("use_residential_as_postal", "");
					editor.putString("postal_address", e.getString("POSTALADDRESS"));
					editor.putString("postal_city", e.getString("POSTALCITY"));
					editor.putString("postal_postcode", e.getString("POSTALPOSTCODE"));
					String dob = e.getString("DOB");
					if (dob.equals("") || (dob.indexOf("-") == -1 && dob.indexOf("/") == -1)) {
						dob = " -  - ";
					}
					editor.putString("dob", dob);
					editor.putLong("gender_pos", 0);
					editor.putString("gender_code", e.getString("SEXCODE"));
					editor.putLong("atsic_pos", 0);
					editor.putString("atsic_code", e.getString("ATSI"));
					editor.putString("sms_code", e.getString("SMS"));
					editor.putString("home_phone", e.getString("HOMEPHONE"));
					editor.putString("work_phone", e.getString("WORKPHONE"));
					editor.putString("mobile_phone", e.getString("MOBILEPHONE"));
					editor.putString("medicare_number", e.getString("MEDICARENO"));
					editor.putString("medicare_line_number", e.getString("MEDICARELINENO"));
					String mExp = e.getString("MEDICAREEXPIRY");
					if (mExp.indexOf("/") > -1) {
						String[] separated = mExp.split("/");
						mExp = separated[1]+"-"+separated[0];
					}
					if (mExp.equals("") || (mExp.indexOf("-") == -1 && mExp.indexOf("/") == -1)) {
						mExp = " -  - ";
					}
					Log.i("mExp", mExp);
					editor.putString("medicare_expiry", mExp);
			        editor.putString("pension_expiry",  " -  - ");
					editor.putLong("pension_pos", 0);
					editor.putString("pension_code", e.getString("PENSIONCODE"));
					editor.putString("pension_number", e.getString("PENSIONNO"));
					String pExp = e.getString("PENSIONEXPIRY");
					if (pExp.equals("") || (pExp.indexOf("-") == -1 && pExp.indexOf("/") == -1)) {
						pExp = " -  - ";
					}
					editor.putString("pension_expiry", pExp);
					editor.putLong("dva_pos", 0);
					editor.putString("dva_code",  e.getString("DVACODE"));
					editor.putString("dva_number", e.getString("DVANO"));
					editor.putString("head_of_family", e.getString("HEADOFFAMILYID"));
					editor.putString("email", e.getString("EMAIL"));
					editor.putLong("nok_title_pos", 0);
					editor.putString("nok_title", e.getString("nok_title"));
					editor.putString("nok_first_name", e.getString("nok_first_name"));
					editor.putString("nok_last_name", e.getString("nok_last_name"));
					editor.putString("nok_address", e.getString("nok_address"));
					editor.putString("nok_city", e.getString("nok_city"));
					editor.putString("nok_postcode", e.getString("nok_postcode"));
					editor.putString("nok_phone", e.getString("nok_phone"));
					editor.putString("nok_alternate_phone", e.getString("nok_alternate_phone"));
					editor.putString("nok_relationship", e.getString("nok_relationship"));
					editor.putLong("ec_title_pos", 0);
					editor.putString("ec_title", e.getString("ec_title"));
					editor.putString("ec_first_name", e.getString("ec_first_name"));
					editor.putString("ec_last_name", e.getString("ec_last_name"));
					editor.putString("ec_address", e.getString("ec_address"));
					editor.putString("ec_city", e.getString("ec_city"));
					editor.putString("ec_postcode", e.getString("ec_postcode"));
					editor.putString("ec_phone", e.getString("ec_phone"));
					editor.putString("ec_alternate_phone", e.getString("ec_alternate_phone"));
					editor.putString("ec_relationship", e.getString("ec_relationship"));
					editor.putString("use_nok_as_ec", "");
					Log.i("Patient Details EC", "EC Title = " + e.getString("ec_title"));
					editor.commit();
				}	
		        
		        Intent np = new Intent(this, NewPatientActivity.class);  
				startActivity(np);
				
	        }catch(JSONException e)        {
	        	 Log.e("log_tag", "Error parsing data "+e.toString());
	        }
	        
	       //pd.dismiss();
	        
	    }
	 
	 public void checkStatus() {
	    	
	    	settings = getSharedPreferences(PREFS_NAME, 0);
		    surgery_id = settings.getString("surgery_id", "");
		    
		    if (surgery_id.equals("")) {
		    	AlertDialog.Builder builder = new AlertDialog.Builder(this);

				builder.setTitle("Set Surgery ID");
				builder.setMessage("The surgery's ID number has not been set.\nPlease enter the surgery's ID number.\nThe surgery's ID number can be obtained from DocAppointment.");

				// Set an EditText view to get user input
				final EditText input = new EditText(this);
				input.setLines(1);
				input.setInputType(InputType.TYPE_CLASS_TEXT);
				
				input.setImeActionLabel("Custom text", KeyEvent.KEYCODE_ENTER);

				builder.setView(input);

				builder.setPositiveButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								Editable value = input.getText();
								surgery_id = value.toString();
								SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
								final SharedPreferences.Editor editor = settings.edit();
								editor.putString("surgery_id", surgery_id);
								editor.commit();
								getSurgeryStatus();
							}
						});

				builder.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
					
							public void onClick(DialogInterface dialog,
									int whichButton) {
								dialog.cancel();
								return;
							}
						});

				alert = builder.create();
				alert.show();
				
		    } else {
		    	
		    	getSurgeryStatus();
		    	
		    }
	    	
	    }
	    
	    public void getSurgeryStatus() {
	    	
	    	pd = new ProgressDialog(this);
	        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	        pd.setMessage("Checking surgery status...");
	        pd.show();
	        
	    	Log.i("Getting Surgery Status", "getting surgery status");
	    	Log.i("Getting Surgery Status URL", "https://secure.docappointment.com.au/apps/patient_details_init.php?surgery_id="+surgery_id);
	    	AsyncHttpClient client = new AsyncHttpClient();
	        client.get("https://secure.docappointment.com.au/apps/patient_details_init.php?surgery_id="+surgery_id, new AsyncHttpResponseHandler() {
	            @Override
	            public void onSuccess(String response) {
	            	loadSurgeryStatusJSONResult(response);
	            }
	        });
	    	
	    }
	    
	    public void loadSurgeryStatusJSONResult(String response) {
	 
		    	pd.dismiss();
		    	
		    	JSONObject json = null;
		   	 	
				try {
					json = new JSONObject(response);
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}    
		        
		        try{
		        
		        	JSONArray result = json.getJSONArray("result");
		        	JSONObject e = result.getJSONObject(0);
		        	
		        	settings = getSharedPreferences(PREFS_NAME, 0);
		        	final SharedPreferences.Editor editor = settings.edit();
		        	editor.putString("surgery_code", e.getString("surgery_code"));
		        	editor.putString("use_pracsoft_alt_screen", e.getString("use_pracsoft_alt_screen"));
		        	logo = e.getString("logo");
	        		logo_landscape = e.getString("logo_landscape");
	        		editor.putString("logo", e.getString("logo"));
	        		editor.putString("logo_landscape", e.getString("logo_landscape"));
		        	
		        	editor.commit();
		        	
		        	Log.i("Check surgery status result", result.toString());
		        	
		        	String surgery_code = settings.getString("surgery_code", "");
		    	    Log.i("surgery_code", surgery_code);
		    	    
		    	    information info = new information();
	                info.execute("");
	        		
		    	    
		    	    int number_of_surveys = e.getInt("number_of_surveys");
		        	
		        	if (number_of_surveys == 0) {
		        		
		        		Button theButton = (Button)findViewById(R.id.survey_btn);
		        		theButton.setVisibility(View.GONE);
		        		
		        	}
		        	
		    	    
		        	if (e.getString("status").equals("Expired")) {
		        		
		        		AlertDialog.Builder builder = new AlertDialog.Builder(this);
			      		builder.setTitle("Patient Details App Expired")
			      			   .setMessage("The DocAppointment Patient Details app has expired\nPlease contact DocAppointment to renew your subscription.")
			      		       .setCancelable(false)
			      		       .setNegativeButton("OK", new DialogInterface.OnClickListener() {
			      		           public void onClick(DialogInterface dialog, int id) {
			      		        	 
			      		              dialog.cancel();
			      		              
			      		           }
			      		       });
			      		alert = builder.create();
			      		alert.show();
			      		
		        	} else if (e.getString("status").equals("Expiring")) {
		        		
		        		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		        		builder.setTitle("Patient Details App About To Expire")
		      			   .setMessage("The DocAppointment Patient Details app will expire within one week.\nPlease contact DocAppointment to renew your subscription.")
		      			 .setCancelable(false)
		      		       .setNegativeButton("OK", new DialogInterface.OnClickListener() {
		      		           public void onClick(DialogInterface dialog, int id) {
		      		        	 
		      		              dialog.cancel();
		      		              
		      		           }
		      		       });
			      		alert = builder.create();
			      		alert.show();

		        	}
		        	
		        }catch(JSONException e)        {
		        	 Log.e("log_tag", "Error parsing data "+e.toString());
		        }
		    	
		    }

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch(v.getId()){
			
			case R.id.new_patient_btn:
				setBlankPatient();
				Intent np = new Intent(this, NewPatientActivity.class);  
				startActivity(np);
				break;
				
			case R.id.existing_patient_btn:
				setBlankPatient();
				getExistingPatient();
				break;
				
			case R.id.survey_btn:
				setSurveyPatientID();
				break;
				
			}
		}
		
		public class information extends AsyncTask<String, String, String>
	    {
	        @Override
	        
	        protected String doInBackground(String... arg0) {
	        	
	        	Log.i("doInBackground", "Calling doInBackground...");
	        	
	            try
	            {
	                URL url = new URL(logo);
	                InputStream is = new BufferedInputStream(url.openStream());
	                b = BitmapFactory.decodeStream(is);
	                
	                URL urlLandscape = new URL(logo_landscape);
	                InputStream isl = new BufferedInputStream(urlLandscape.openStream());
	                l = BitmapFactory.decodeStream(isl);

	            } catch(Exception e){}
	            return null;
	        }

	        @Override
	        protected void onPostExecute(String result) {
	        	
	        	String rotation = getRotation(MainActivity.this);
	        	if (rotation.equals("landscape")) {
	        		practiceLogo.setImageBitmap(b);
	        	} else {
	        		practiceLogo.setImageBitmap(l);
	        	}
	        	
	        }
	    }
		
		public String getRotation(Context context){
		    final int rotation = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getRotation();
		           switch (rotation) {
		            case Surface.ROTATION_0:
		                return "portrait";
		            case Surface.ROTATION_90:
		                return "landscape";
		            case Surface.ROTATION_180:
		                return "reverse portrait";
		            default:
		                return "reverse landscape";
		            }
		        }
		
		@Override
		public void onConfigurationChanged(Configuration newConfig) {
		    super.onConfigurationChanged(newConfig);  
		    information info = new information();
	        info.execute("");
		}

}
