package com.FirstAvailable.PatientDetails;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.URL;

import org.json.JSONArray;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.Surface;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

public class CompleteDetailsActivity extends Activity implements OnClickListener {
	
	public JSONArray surveys_array;
	public ProgressDialog pd;
	public static final String PREFS_NAME = "MyPrefsFile";
	public static SharedPreferences settings = null;
	public static String surgery_code;
	public Spinner surveysSpinner;
	private AlertDialog alert;
	public static String surgery_id;
	public static String logo;
	public static String logo_landscape;
	
	Bitmap b;
    ImageView practiceLogo;
    
    Bitmap l;
    ImageView practiceLogoLandscape;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.complete_details);
		
		Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(this.getResources().getColor(R.color.red));
        
        practiceLogo = (ImageView)findViewById(R.id.practiceLogo);
        settings = getSharedPreferences(PREFS_NAME, 0);
        logo = settings.getString("logo", "");
        logo_landscape = settings.getString("logo_landscape", "");
        
        information info = new information();
        info.execute("");
		
	    View checkPatientDetailsBtnClick = findViewById(R.id.complete_details_btn);
	    checkPatientDetailsBtnClick.setOnClickListener(this);
	    
	    SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        String patient_id = settings.getString("patient_id", "");
        
    		ImageView heroIcon = (ImageView) findViewById(R.id.heroImage);
    		if (patient_id == "") {
    			heroIcon.setImageResource(R.drawable.hero_new_patient);
			} else {
				heroIcon.setImageResource(R.drawable.hero_existing_patient);
			}
    		
	  
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void goToPatientDetails() {
		
		settings = getSharedPreferences(PREFS_NAME, 0);
		String atsic_code = settings.getString("atsic_code", "");
	    surgery_id = settings.getString("surgery_id", "");
	    
		if (!atsic_code.equals("4") && surgery_id.equals("142")) {
			String message = "Please register the patient manually and ensure that an Indigenous Health Incentive form has been signed.";
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    	builder.setTitle("Indigenous Patient")
				   .setMessage(message)
			       .setCancelable(false)
			       .setNegativeButton("OK", new DialogInterface.OnClickListener() {
			           public void onClick(DialogInterface dialog, int id) {
			        	   Context context = getApplicationContext();
			        	   Intent sa = new Intent(context, NewPatientActivity.class);  
			   			   startActivity(sa);
			           }
			       });
	    	
	  		builder.create();
	  		alert = builder.create();
	  		alert.show();
		} else {
			Intent sa = new Intent(this, NewPatientActivity.class);  
			startActivity(sa);
		}
	}
	
	public void checkPatientDetails() {
    	
		settings = getSharedPreferences(PREFS_NAME, 0);
		surgery_code = settings.getString("surgery_code", "");
		surgery_code = surgery_code.trim();
	    
		Log.i("surgery code", surgery_code + " length = " + surgery_code.length());
		
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	final SharedPreferences.Editor editor = settings.edit();
    	editor.putString("survey_answers", "");
		editor.commit();
		builder.setTitle("Check Patient Details");
		builder.setMessage("The patient has finished entering or updating their details. To check the patient's details enter your surgery's security code.");

		// Set an EditText view to get user input
		final EditText input = new EditText(this);
		input.setLines(1);
		input.setInputType(InputType.TYPE_CLASS_TEXT);
		
		input.setImeActionLabel("Custom text", KeyEvent.KEYCODE_ENTER);

		builder.setView(input);

		builder.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,
							int whichButton) {
						Editable value = input.getText();
						String entered_id = value.toString();
						entered_id = entered_id.trim();
						Log.i("entered_id", entered_id + " length = " + entered_id.length());
						Log.i("entered_id == surgery_code", entered_id + " == " + surgery_code + " - " + entered_id.compareTo(surgery_code));
						if (entered_id.compareTo(surgery_code) == 0) {
							editor.putString("patient_details_checked", "yes");
							editor.commit();
							goToPatientDetails();
						} else {
							Context context = getApplicationContext();
							CharSequence text = "The surgey security code is incorrect. Please try again.";
							int duration = Toast.LENGTH_LONG;
								
							Toast toast = Toast.makeText(context, text, duration);
							toast.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL, 0, 0);
							toast.show();
							return;
						}
						
					}
				});

		builder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
			
					public void onClick(DialogInterface dialog,
							int whichButton) {
						return;
					}
				});

		alert = builder.create();
		alert.show();
	
	}
	
    @Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		
		case R.id.complete_details_btn:
			checkPatientDetails();
		break;
			
		}
	}
    
    public class information extends AsyncTask<String, String, String>
    {
        @Override
        protected String doInBackground(String... arg0) {

            try
            {
                URL url = new URL(logo);
                InputStream is = new BufferedInputStream(url.openStream());
                b = BitmapFactory.decodeStream(is);
                
                URL urlLandscape = new URL(logo_landscape);
                InputStream isl = new BufferedInputStream(urlLandscape.openStream());
                l = BitmapFactory.decodeStream(isl);

            } catch(Exception e){}
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
        	
        	String rotation = getRotation(CompleteDetailsActivity.this);
        	
        	if (rotation.equals("landscape")) {
        		practiceLogo.setImageBitmap(b);
        	} else {
        		practiceLogo.setImageBitmap(l);
        	}
        	
        }
    }
	
	public String getRotation(Context context){
	
	    final int rotation = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getRotation();
	           switch (rotation) {
	            case Surface.ROTATION_0:
	                return "portrait";
	            case Surface.ROTATION_90:
	                return "landscape";
	            case Surface.ROTATION_180:
	                return "portrait";
	            default:
	                return "portrait";
	            }
	        }
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		Log.i("onConfigurationChanged", "onConfigurationChanged" );
	    super.onConfigurationChanged(newConfig);  
	    information info = new information();
        info.execute("");
	}
}
