package com.FirstAvailable.PatientDetails;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

public class PatientDetailsFragment3 extends Fragment {
	
	public static final String PREFS_NAME = "MyPrefsFile";
	public static SharedPreferences settings = null;
	SharedPreferences.Editor editor;
	public Button btnUseResAsPos;
	public static String use_residential_as_postal;
	
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {
	        return inflater.inflate(R.layout.patient_details_3, container, false);
	    }

		@Override
	    public void onStop() {
	    // TODO Auto-generated method stub
	        super.onStop();

	        Log.i("onStop", "stopping step 1");
	        
	        EditText pa = ((EditText) getView().findViewById(R.id.postal_address));
		    EditText pc = ((EditText) getView().findViewById(R.id.postal_city));
		    EditText pp = ((EditText) getView().findViewById(R.id.postal_postcode));
	        
		    String postal_address = pa.getText().toString();
			String postal_city = pc.getText().toString();
			String postal_postcode = pp.getText().toString();
			
			editor.putString("postal_address", postal_address);
			editor.putString("postal_city", postal_city);
			editor.putString("postal_postcode", postal_postcode);
			
			editor.putString("use_residential_as_postal", use_residential_as_postal);
		       
			editor.commit();
			
			Log.i("use_residential_as_postal", " - " + settings.getString("use_residential_as_postal", ""));
			
			Log.i("postal_address", " - " + settings.getString("postal_address", ""));
			Log.i("postal_city", " - " + settings.getString("postal_city", ""));
			Log.i("postal_postcode", " - " + settings.getString("postal_postcode", ""));
			 
		}
		
		@Override
		public void onViewCreated(View view, Bundle savedInstanceState) {
	
		    super.onViewCreated(view, savedInstanceState);
		    
		    settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
		    editor = settings.edit();
		    
		    use_residential_as_postal = settings.getString("use_residential_as_postal", "");
		    Log.i("use_residential_as_postal", use_residential_as_postal);
		    
		    btnUseResAsPos = (Button) getView().findViewById(R.id.btnUseResAsPos);
			
		    btnUseResAsPos.setOnClickListener(new View.OnClickListener() {
	        	public void onClick(View v) {
	        		setResAsPos();	
	        	}
	        });
			
		    if (use_residential_as_postal.equals("1")) {
		    	btnUseResAsPos.setBackgroundResource(R.drawable.residential_as_postal_checked);
	    	} else {
	    		btnUseResAsPos.setBackgroundResource(R.drawable.residential_as_postal_unchecked);
	    	}
			
			String postal_address = settings.getString("postal_address", "");
			String postal_city = settings.getString("postal_city", "");
			String postal_postcode = settings.getString("postal_postcode", "");
			
			EditText pa = ((EditText) getView().findViewById(R.id.postal_address));
			pa.setText(postal_address);
			pa.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
		    EditText pc = ((EditText) getView().findViewById(R.id.postal_city));
		    pc.setText(postal_city);
		    pc.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
		    EditText pp = ((EditText) getView().findViewById(R.id.postal_postcode));
		    pp.setText(postal_postcode);
		    pp.setInputType(InputType.TYPE_CLASS_NUMBER);
		    
	    }
		
		public void setResAsPosFields() {
			
			EditText pa = ((EditText) getView().findViewById(R.id.postal_address));
		    EditText pc = ((EditText) getView().findViewById(R.id.postal_city));
		    EditText pp = ((EditText) getView().findViewById(R.id.postal_postcode));
	        
			if (use_residential_as_postal.equals("1")) {
		    	
				editor.putString("postal_address", settings.getString("address_1", ""));
				editor.putString("postal_city", settings.getString("city", ""));
				editor.putString("postal_postcode", settings.getString("postcode", ""));
				
				editor.commit();
				
	    	} 
			
			pa.setText(settings.getString("postal_address", ""));
    		pc.setText(settings.getString("postal_city", ""));
    		pp.setText(settings.getString("postal_postcode", ""));
    		
			Log.i("postal_address", " - " + settings.getString("postal_address", ""));
			Log.i("postal_city", " - " + settings.getString("postal_city", ""));
			Log.i("postal_postcode", " - " + settings.getString("postal_postcode", ""));

		}
		
		public void hideKeyboard() {
	    	InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
	    	View view = getActivity().getCurrentFocus();
	        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
	        //View mainView = getView().findViewById(R.id.pdLayout);
	        //mainView.requestFocus();
	    }
		
		public void setResAsPos() {
	    	hideKeyboard();
	    	if (use_residential_as_postal.equals("")) {
	    		use_residential_as_postal = "1";
	    		btnUseResAsPos.setBackgroundResource(R.drawable.residential_as_postal_checked);
	    	} else {
	    		use_residential_as_postal = "";
	    		btnUseResAsPos.setBackgroundResource(R.drawable.residential_as_postal_unchecked);
	    	}
	    	setResAsPosFields();
	    }
		
		public boolean validateInput() {
			
			EditText pp = ((EditText) getView().findViewById(R.id.postal_postcode));
			
			String postcode = pp.getText().toString();
			
			String postCodeRegex = "^(0[289][0-9]{2})|([1345689][0-9]{3})|(2[0-8][0-9]{2})|(290[0-9])|(291[0-4])|(7[0-4][0-9]{2})|(7[8-9][0-9]{2})$";
			
			if(!postcode.matches(postCodeRegex) && postcode.length() > 0) {
				return false;
			}
			
			return true;
		}
}
