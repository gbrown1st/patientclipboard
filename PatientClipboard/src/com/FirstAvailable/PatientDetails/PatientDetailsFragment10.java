package com.FirstAvailable.PatientDetails;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

public class PatientDetailsFragment10 extends Fragment {
	
	public static final String PREFS_NAME = "MyPrefsFile";
	public static SharedPreferences settings = null;
	SharedPreferences.Editor editor;
	private CheckBox cardNo, cardYes, allergiesNo, allergiesYes, medicationsNo, medicationsYes, conditionsNo, conditionsYes, historyNo, historyYes, exSmoker, nonSmoker, smoker, walkedPast, internet, personToldMe, leaflet, facebook;
	private EditText allergies, medications, conditions, history, smokesPerDay;
	
	Button button;
	
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {
			
	        return inflater.inflate(R.layout.patient_details_10, container, false);
	    }

		@Override
	    public void onStop() {
	    // TODO Auto-generated method stub
	        super.onStop();

	        Log.i("onStop", "stopping step medical details");
	        
			EditText a = ((EditText) getView().findViewById(R.id.allergies));
	        EditText m = ((EditText) getView().findViewById(R.id.medications));
	        EditText c = ((EditText) getView().findViewById(R.id.conditions));
	        EditText f = ((EditText) getView().findViewById(R.id.history));
	        EditText s = ((EditText) getView().findViewById(R.id.smokesPerDay));
	        EditText o = ((EditText) getView().findViewById(R.id.hearOther));
		   
		    String allergies_text = a.getText().toString();
		    String medications_text = m.getText().toString();
		    String medical_conditions_text = c.getText().toString();
		    String family_history_text = f.getText().toString();
		    String smokes_per_day_text = s.getText().toString();
		    String card_text = "";
		    String non_smoker_text = "";
		    String smoker_text = "";
		    String ex_smoker_text = "";
		    String hear_text = "";
		    String hear_other_text = o.getText().toString();
		    
		    allergiesNo = (CheckBox) getView().findViewById(R.id.allergiesNo);
			allergiesYes = (CheckBox) getView().findViewById(R.id.allergiesYes);
			medicationsNo = (CheckBox) getView().findViewById(R.id.medicationsNo);
			medicationsYes = (CheckBox) getView().findViewById(R.id.medicationsYes);
			conditionsNo = (CheckBox) getView().findViewById(R.id.conditionsNo);
			conditionsYes = (CheckBox) getView().findViewById(R.id.conditionsYes);
			historyNo = (CheckBox) getView().findViewById(R.id.historyNo);
			historyYes = (CheckBox) getView().findViewById(R.id.historyYes);
		    
		    cardYes = (CheckBox) getView().findViewById(R.id.cardYes);
		    nonSmoker = (CheckBox) getView().findViewById(R.id.nonSmoker);
		    exSmoker = (CheckBox) getView().findViewById(R.id.exSmoker);
			smoker = (CheckBox) getView().findViewById(R.id.smoker);
			
			if (!allergiesNo.isChecked() && !allergiesYes.isChecked()) {
				allergies_text = "none entered";
			}
			
			if (allergiesNo.isChecked() && !allergiesYes.isChecked()) {
				allergies_text = "nil";
			}
			
			if (!medicationsNo.isChecked() && !medicationsYes.isChecked()) {
				medications_text = "none entered";
			}
			
			if (medicationsNo.isChecked() && !medicationsYes.isChecked()) {
				medications_text = "nil";
			}
			
			if (!conditionsNo.isChecked() && !conditionsYes.isChecked()) {
				medical_conditions_text = "none entered";
			}
			
			if (conditionsNo.isChecked() && !conditionsYes.isChecked()) {
				medical_conditions_text = "nil";
			}
			
			if (!historyNo.isChecked() && !historyYes.isChecked()) {
				family_history_text = "none entered";
			}
			
			if (historyNo.isChecked() && !historyYes.isChecked()) {
				family_history_text = "nil";
			}
			
			if (cardYes.isChecked()) {
				card_text = "Y";
			}
			
			if (cardNo.isChecked()) {
				card_text = "N";
			}
			
			if (nonSmoker.isChecked()) {
				non_smoker_text = "Y";
			}
		
			if (exSmoker.isChecked()) {
				ex_smoker_text = "Y";
			}
			
			if (smoker.isChecked()) {
				smoker_text = "Y";
			}
			
			walkedPast = (CheckBox) getView().findViewById(R.id.walkedPast);
			internet = (CheckBox) getView().findViewById(R.id.internet);
			personToldMe = (CheckBox) getView().findViewById(R.id.personToldMe);
			leaflet = (CheckBox) getView().findViewById(R.id.leaflet);
			facebook = (CheckBox) getView().findViewById(R.id.facebook);
			
			if (walkedPast.isChecked()) {
				hear_text += "Walked past ";
			}
			
			if (internet.isChecked()) {
				hear_text += "Internet ";
			}
			
			if (personToldMe.isChecked()) {
				hear_text += "Person told me ";
			}
			
			if (leaflet.isChecked()) {
				hear_text += "Leaflet ";
			}
			
			if (facebook.isChecked()) {
				hear_text += "Facebook ";
			}
			
			editor.putString("card", card_text);
			editor.putString("allergies", allergies_text);
			editor.putString("medications", medications_text);
			editor.putString("medical_conditions", medical_conditions_text);
			editor.putString("family_history", family_history_text);
			editor.putString("non_smoker", non_smoker_text);
			editor.putString("smoker", smoker_text);
			editor.putString("smokes_per_day", smokes_per_day_text);
			editor.putString("ex_smoker", ex_smoker_text);
	        editor.putString("hear", hear_text);
	        editor.putString("hear_other", hear_other_text);
	       
			editor.commit();
			
			Log.i("card", " - " + settings.getString("card", ""));
			Log.i("allergies", " - " + settings.getString("allergies", ""));
			Log.i("medications", " - " + settings.getString("medications", ""));
			Log.i("medical_conditions", " - " + settings.getString("medical_conditions", ""));
			Log.i("family_history", " - " + settings.getString("family_history", ""));
			Log.i("non_smoker", " - " + settings.getString("non_smoker", ""));
			Log.i("smoker", " - " + settings.getString("smoker", ""));
			Log.i("smokes_per_day", " - " + settings.getString("smokes_per_day", ""));
			Log.i("ex_smoker", " - " + settings.getString("ex_smoker", ""));
			Log.i("hear", " - " + settings.getString("hear", ""));
			Log.i("hear_other", " - " + settings.getString("hear_other", ""));
			 
		}
		
		@Override
		public void onViewCreated(View view, Bundle savedInstanceState) {
	
		    super.onViewCreated(view, savedInstanceState);
		    settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
		    editor = settings.edit();
		    addListenerOnChk();
		    
		    EditText a = ((EditText) getView().findViewById(R.id.allergies));
	        EditText m = ((EditText) getView().findViewById(R.id.medications));
	        EditText c = ((EditText) getView().findViewById(R.id.conditions));
	        EditText f = ((EditText) getView().findViewById(R.id.history));
	        EditText s = ((EditText) getView().findViewById(R.id.smokesPerDay));
	        EditText o = ((EditText) getView().findViewById(R.id.hearOther));
	        
	        cardNo = (CheckBox) getView().findViewById(R.id.cardNo);
			cardYes = (CheckBox) getView().findViewById(R.id.cardYes);
			
			allergiesNo = (CheckBox) getView().findViewById(R.id.allergiesNo);
			allergiesYes = (CheckBox) getView().findViewById(R.id.allergiesYes);
			
			medicationsNo = (CheckBox) getView().findViewById(R.id.medicationsNo);
			medicationsYes = (CheckBox) getView().findViewById(R.id.medicationsYes);
			
			conditionsNo = (CheckBox) getView().findViewById(R.id.conditionsNo);
			conditionsYes = (CheckBox) getView().findViewById(R.id.conditionsYes);
			
			historyNo = (CheckBox) getView().findViewById(R.id.historyNo);
			historyYes = (CheckBox) getView().findViewById(R.id.historyYes);
			
			nonSmoker = (CheckBox) getView().findViewById(R.id.nonSmoker);
			exSmoker = (CheckBox) getView().findViewById(R.id.exSmoker);
			smoker = (CheckBox) getView().findViewById(R.id.smoker);
			
			walkedPast = (CheckBox) getView().findViewById(R.id.walkedPast);
			internet = (CheckBox) getView().findViewById(R.id.internet);
			personToldMe = (CheckBox) getView().findViewById(R.id.personToldMe);
			leaflet = (CheckBox) getView().findViewById(R.id.leaflet);
			facebook = (CheckBox) getView().findViewById(R.id.facebook);
	        
			String card_text = settings.getString("card", "");
		    String allergies_text = settings.getString("allergies", "");
		    String medications_text = settings.getString("medications", "");
		    m.setText(medications_text);
		    String medical_conditions_text = settings.getString("medical_conditions", "");
		    c.setText(medical_conditions_text);
		    String family_history_text = settings.getString("family_history", "");
		    f.setText(family_history_text);
		    String non_smoker_text = settings.getString("non_smoker", "");
		    String ex_smoker_text = settings.getString("ex_smoker", "");
		    String smoker_text = settings.getString("smoker", "");
		    String smokes_per_day_text = settings.getString("smokes_per_day", "");
		    String hear_text = settings.getString("hear", "");
		    String hear_other_text = settings.getString("hear_other", "");
		    o.setText(hear_other_text);
		    
		    if (card_text.equals("N")) {
		    	cardNo.setChecked(true);
		    } 
		    
		    if (card_text.equals("Y")) {
		    	cardYes.setChecked(true);
		    } 

		    if (!allergies_text.equals("") && !allergies_text.equals("nil")) {
		    	allergiesYes.setChecked(true);
		    	a.setText(allergies_text);
		    	a.setEnabled(true);
		    }
		    
		    if (allergies_text.equals("nil")) {
		    	allergiesNo.setChecked(true);
		    	a.setText("");
		    	a.setEnabled(false);
		    }
		    
		    if (!medications_text.equals("") && !medications_text.equals("nil")) {
		    	medicationsYes.setChecked(true);
		    	m.setText(medications_text);
		    	m.setEnabled(true);
		    }
		    
		    if (medications_text.equals("nil")) {
		    	medicationsNo.setChecked(true);
		    	m.setText("");
		    	m.setEnabled(false);
		    }
		    
		    if (!medical_conditions_text.equals("") && !medical_conditions_text.equals("nil")) {
		    	conditionsYes.setChecked(true);
		    	c.setText(medical_conditions_text);
		    	c.setEnabled(true);
		    }
		    
		    if (medical_conditions_text.equals("nil")) {
		    	conditionsNo.setChecked(true);
		    	c.setText("");
		    	c.setEnabled(false);
		    }
		    
		    if (!family_history_text.equals("") && !family_history_text.equals("nil")) {
		    	historyYes.setChecked(true);
		    	f.setText(family_history_text);
		    	f.setEnabled(true);
		    }
		    
		    if (family_history_text.equals("nil")) {
		    	historyNo.setChecked(true);
		    	f.setText("");
		    	f.setEnabled(false);
		    }
		    
		    if (non_smoker_text.equals("Y")) {
		    	nonSmoker.setChecked(true);
		    } 
		    
		    if (ex_smoker_text.equals("Y")) {
		    	exSmoker.setChecked(true);
		    } 
		    
		    if (smoker_text.equals("Y")) {
		    	smoker.setChecked(true);
		    	s.setText(smokes_per_day_text);
		    	s.setEnabled(true);
		    } 
		    
		    if (hear_text.contains("Walked past ")) {
		    	walkedPast.setChecked(true);
		    } 
		    
		    if (hear_text.contains("Internet ")) {
		    	internet.setChecked(true);
		    } 
		    
		    if (hear_text.contains("Person told me ")) {
		    	personToldMe.setChecked(true);
		    } 
		    
		    if (hear_text.contains("Leaflet ")) {
		    	leaflet.setChecked(true);
		    } 
		    
		    if (hear_text.contains("Facebook ")) {
		    	facebook.setChecked(true);
		    } 
		    
	    }
		
		public void addListenerOnChk() {

			cardNo = (CheckBox) getView().findViewById(R.id.cardNo);
			cardNo.setChecked(false);
			cardYes = (CheckBox) getView().findViewById(R.id.cardYes);
			cardYes.setChecked(false);
			
			allergiesNo = (CheckBox) getView().findViewById(R.id.allergiesNo);
			allergiesNo.setChecked(false);
			allergiesYes = (CheckBox) getView().findViewById(R.id.allergiesYes);
			allergiesYes.setChecked(false);
			allergies = (EditText) getView().findViewById(R.id.allergies);
			allergies.setEnabled(false);
			
			medicationsNo = (CheckBox) getView().findViewById(R.id.medicationsNo);
			medicationsNo.setChecked(false);
			medicationsYes = (CheckBox) getView().findViewById(R.id.medicationsYes);
			medicationsYes.setChecked(false);
			medications = (EditText) getView().findViewById(R.id.medications);
			medications.setEnabled(false);
			
			conditionsNo = (CheckBox) getView().findViewById(R.id.conditionsNo);
			conditionsNo.setChecked(false);
			conditionsYes = (CheckBox) getView().findViewById(R.id.conditionsYes);
			conditionsYes.setChecked(false);
			conditions = (EditText) getView().findViewById(R.id.conditions);
			conditions.setEnabled(false);
			
			historyNo = (CheckBox) getView().findViewById(R.id.historyNo);
			historyNo.setChecked(false);
			historyYes = (CheckBox) getView().findViewById(R.id.historyYes);
			historyYes.setChecked(false);
			history = (EditText) getView().findViewById(R.id.history);
			history.setEnabled(false);
			
			nonSmoker = (CheckBox) getView().findViewById(R.id.nonSmoker);
			nonSmoker.setChecked(false);
			exSmoker = (CheckBox) getView().findViewById(R.id.exSmoker);
			exSmoker.setChecked(false);
			smoker = (CheckBox) getView().findViewById(R.id.smoker);
			smoker.setChecked(false);
			smokesPerDay = (EditText) getView().findViewById(R.id.smokesPerDay);
			smokesPerDay.setEnabled(false);
			
			cardNo.setOnClickListener(new OnClickListener() {

				  @Override
				  public void onClick(View v) {
				        //is chkIos checked?
						if (((CheckBox) v).isChecked()) {
							cardYes.setChecked(false);
						} 
				  }
				});
			
			cardYes.setOnClickListener(new OnClickListener() {

				  @Override
				  public void onClick(View v) {
				        //is chkIos checked?
						if (((CheckBox) v).isChecked()) {
							cardNo.setChecked(false);
						} 
				  }
				});
			
			allergiesNo.setOnClickListener(new OnClickListener() {

				  @Override
				  public void onClick(View v) {
				        //is chkIos checked?
						if (((CheckBox) v).isChecked()) {
							allergiesYes.setChecked(false);
							allergies.setEnabled(false);
							allergies.setHint("");
						} else {
							if (!allergiesYes.isChecked()){
								allergies.setHint("");
							}
						}
				  }
				});
			
			allergiesYes.setOnClickListener(new OnClickListener() {

				  @Override
				  public void onClick(View v) {
			                //is chkIos checked?
					if (((CheckBox) v).isChecked()) {
						allergiesNo.setChecked(false);
						allergies.setEnabled(true);
						allergies.setHint("TAP HERE");
					} else {
						if (!allergiesNo.isChecked()){
							allergies.setHint("");
						}
					}

				  }
				});
			
			medicationsNo.setOnClickListener(new OnClickListener() {

				  @Override
				  public void onClick(View v) {
				        //is chkIos checked?
						if (((CheckBox) v).isChecked()) {
							medicationsYes.setChecked(false);
							medications.setEnabled(false);
							medications.setHint("");
						} else {
							if (!medicationsYes.isChecked()){
								medications.setHint("");
							}
						}
				  }
				});
			
			medicationsYes.setOnClickListener(new OnClickListener() {

				  @Override
				  public void onClick(View v) {
			                //is chkIos checked?
					if (((CheckBox) v).isChecked()) {
						medicationsNo.setChecked(false);
						medications.setEnabled(true);
						medications.setHint("TAP HERE");
					} else {
						if (!medicationsNo.isChecked()){
							medications.setHint("");
						}
					}

				  }
				});
			
			conditionsNo.setOnClickListener(new OnClickListener() {

				  @Override
				  public void onClick(View v) {
				        //is chkIos checked?
						if (((CheckBox) v).isChecked()) {
							conditionsYes.setChecked(false);
							conditions.setEnabled(false);
							conditions.setHint("");
						} else {
							if (!conditionsYes.isChecked()){
								conditions.setHint("");
							}
						}
				  }
				});
			
			conditionsYes.setOnClickListener(new OnClickListener() {

				  @Override
				  public void onClick(View v) {
			                //is chkIos checked?
					if (((CheckBox) v).isChecked()) {
						conditionsNo.setChecked(false);
						conditions.setEnabled(true);
						conditions.setHint("TAP HERE");
					} else {
						if (!conditionsNo.isChecked()){
							conditions.setHint("");
						}
					}

				  }
				});
			
			historyNo.setOnClickListener(new OnClickListener() {

				  @Override
				  public void onClick(View v) {
				        //is chkIos checked?
						if (((CheckBox) v).isChecked()) {
							historyYes.setChecked(false);
							history.setEnabled(false);
							history.setHint("");
						} else {
							if (!historyYes.isChecked()){
								history.setHint("");
							}
						}
				  }
				});
			
			historyYes.setOnClickListener(new OnClickListener() {

				  @Override
				  public void onClick(View v) {
			                //is chkIos checked?
					if (((CheckBox) v).isChecked()) {
						historyNo.setChecked(false);
						history.setEnabled(true);
						history.setHint("TAP HERE");
					} else {
						if (!historyNo.isChecked()){
							history.setHint("");
						}
					}

				  }
				});
			
			exSmoker.setOnClickListener(new OnClickListener() {

				  @Override
				  public void onClick(View v) {
			                //is chkIos checked?
					if (((CheckBox) v).isChecked()) {
						nonSmoker.setChecked(false);
						smoker.setChecked(false);
						smokesPerDay.setText("");
						smokesPerDay.setEnabled(false);
						smokesPerDay.setHint("");
					} else {
						if (!nonSmoker.isChecked() && !smoker.isChecked()){
							smokesPerDay.setText("");
							smokesPerDay.setEnabled(false);
							smokesPerDay.setHint("");
						}
					}

				  }
				});
			
			nonSmoker.setOnClickListener(new OnClickListener() {

				  @Override
				  public void onClick(View v) {
			                //is chkIos checked?
					if (((CheckBox) v).isChecked()) {
						exSmoker.setChecked(false);
						smoker.setChecked(false);
						smokesPerDay.setText("");
						smokesPerDay.setEnabled(false);
						smokesPerDay.setHint("");
					} else {
						if (!exSmoker.isChecked() && !smoker.isChecked()){
							smokesPerDay.setText("");
							smokesPerDay.setEnabled(false);
							smokesPerDay.setHint("");
						}
					}

				  }
				});
			
			smoker.setOnClickListener(new OnClickListener() {

				  @Override
				  public void onClick(View v) {
			                //is chkIos checked?
					if (((CheckBox) v).isChecked()) {
						exSmoker.setChecked(false);
						nonSmoker.setChecked(false);
						smokesPerDay.setEnabled(true);
						smokesPerDay.setHint("TAP HERE");
					} else {
						if (!exSmoker.isChecked() && !nonSmoker.isChecked()){
							smokesPerDay.setText("");
							smokesPerDay.setEnabled(false);
							smokesPerDay.setHint("");
						}
					}

				  }
				});

			  }
		
}
