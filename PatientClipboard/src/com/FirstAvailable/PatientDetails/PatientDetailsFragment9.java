package com.FirstAvailable.PatientDetails;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Spinner;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class PatientDetailsFragment9 extends Fragment {
	
	public JSONArray titles_array;
	public ProgressDialog pd;
	public static final String PREFS_NAME = "MyPrefsFile";
	public static SharedPreferences settings = null;
	public static String surgery_id;
	public Spinner titlesSpinner;
	SharedPreferences.Editor editor;
	public Button NOKAsECButton;
	public static String use_nok_as_ec;
	
	Button button;
	
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {
			
	        return inflater.inflate(R.layout.patient_details_9, container, false);
	    }

		@Override
	    public void onStop() {
	    // TODO Auto-generated method stub
	        super.onStop();

	        Log.i("onStop", "stopping step 9");
	        
			EditText fn = ((EditText) getView().findViewById(R.id.ec_first_name));
		    EditText ln = ((EditText) getView().findViewById(R.id.ec_last_name));
		    EditText a = ((EditText) getView().findViewById(R.id.ec_address));
		    EditText c = ((EditText) getView().findViewById(R.id.ec_city));
		    EditText pc = ((EditText) getView().findViewById(R.id.ec_postcode));
		    EditText ph = ((EditText) getView().findViewById(R.id.ec_phone));
		    EditText aph = ((EditText) getView().findViewById(R.id.ec_alternate_phone));
		    EditText r = ((EditText) getView().findViewById(R.id.ec_relationship));
	        
		    String ec_first_name = fn.getText().toString();
			String ec_last_name = ln.getText().toString();
			String ec_address = a.getText().toString();
			String ec_city = c.getText().toString();
			String ec_postcode = pc.getText().toString();
			String ec_phone = ph.getText().toString();
			String ec_alternate_phone = aph.getText().toString();
			String ec_relationship = r.getText().toString();
			
			editor.putString("ec_first_name", ec_first_name);
			editor.putString("ec_last_name", ec_last_name);
			editor.putString("ec_address", ec_address);
			editor.putString("ec_city", ec_city);
			editor.putString("ec_postcode", ec_postcode);
			editor.putString("ec_phone", ec_phone);
			editor.putString("ec_alternate_phone", ec_alternate_phone);
			editor.putString("ec_relationship", ec_relationship);
			
	        editor.putString("use_nok_as_ec", use_nok_as_ec);
	       
			editor.commit();
			
			Log.i("use_nok_as_ec", " - " + settings.getString("use_nok_as_ec", ""));
			
			Log.i("ec_first_name", " - " + settings.getString("ec_first_name", ""));
			Log.i("ec_last_name", " - " + settings.getString("ec_last_name", ""));
			Log.i("ec_address", " - " + settings.getString("ec_address", ""));
			Log.i("ec_city", " - " + settings.getString("ec_city", ""));
			Log.i("ec_postcode", " - " + settings.getString("ec_postcode", ""));
			Log.i("ec_phone", " - " + settings.getString("ec_phone", ""));
			Log.i("ec_alternate_phone", " - " + settings.getString("ec_alternate_phone", ""));
			Log.i("ec_relationship", " - " + settings.getString("ec_relationship", ""));
			 
		}
		
		@Override
		public void onViewCreated(View view, Bundle savedInstanceState) {
	
		    super.onViewCreated(view, savedInstanceState);
		   
		    settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
		    editor = settings.edit();
		    
		    surgery_id = settings.getString("surgery_id", "");
		    Log.i("surgery_id", surgery_id);
		    
		    use_nok_as_ec = settings.getString("use_nok_as_ec", "");
		    Log.i("use_nok_as_ec", use_nok_as_ec);
		    
		    titlesSpinner = (Spinner) getView().findViewById(R.id.ec_titles_spinner);
		    
		    NOKAsECButton = (Button) getView().findViewById(R.id.btnUseResAsPos);
			
		    NOKAsECButton.setOnClickListener(new View.OnClickListener() {
	        	public void onClick(View v) {
	        		setNOKASEC();	
	        	}
	        });
			
		    if (use_nok_as_ec.equals("1")) {
	    		NOKAsECButton.setBackgroundResource(R.drawable.nok_as_ec_checked);
	    	} else {
	    		NOKAsECButton.setBackgroundResource(R.drawable.nok_as_ec_unchecked);
	    	}
		    
		    String titles_json = settings.getString("titles_json", "");
		    
		    if (titles_json.length() == 0) {
				getTitles();
		    } else {
		    	loadTitlesJSONResult(titles_json);
		    }
		    //Log.i("titles_json", titles_json);
		    
		    titlesSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
	    	    @Override
	    	    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
	    
					try {
						JSONObject selectedTitle = titles_array.getJSONObject(position);
						String title = selectedTitle.getString("title");
						String title_code = selectedTitle.getString("title_code");
						
						editor.putLong("ec_title_pos", position);
						editor.putString("ec_title", title);
						editor.putString("ec_title_code", title_code);
						editor.commit();
						
						Log.i("Object", "title_code " + title_code);
						Log.i("Object", "ec_title_pos " + position);
						
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
	    	    }
	    	    

	    	    @Override
	    	    public void onNothingSelected(AdapterView<?> parentView) {
	    	        // your code here
	    	    }

	    	});
			
		    EditText fn = ((EditText) getView().findViewById(R.id.ec_first_name));
		    EditText ln = ((EditText) getView().findViewById(R.id.ec_last_name));
		    EditText a = ((EditText) getView().findViewById(R.id.ec_address));
		    EditText c = ((EditText) getView().findViewById(R.id.ec_city));
		    EditText pc = ((EditText) getView().findViewById(R.id.ec_postcode));
		    EditText ph = ((EditText) getView().findViewById(R.id.ec_phone));
		    EditText aph = ((EditText) getView().findViewById(R.id.ec_alternate_phone));
		    EditText r = ((EditText) getView().findViewById(R.id.ec_relationship));
		    
		    fn.setText(settings.getString("ec_first_name", ""));
		    fn.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
			ln.setText(settings.getString("ec_last_name", ""));
			ln.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
			a.setText(settings.getString("ec_address", ""));
			a.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
			c.setText(settings.getString("ec_city", ""));
			c.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
			pc.setText(settings.getString("ec_postcode", ""));
			pc.setInputType(InputType.TYPE_CLASS_NUMBER);
			ph.setText(settings.getString("ec_phone", ""));
			ph.setInputType(InputType.TYPE_CLASS_PHONE);
			aph.setText(settings.getString("ec_alternate_phone", ""));
			aph.setInputType(InputType.TYPE_CLASS_PHONE);
			r.setText(settings.getString("ec_relationship", ""));
			r.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
			
		    //hideKeyboard();
		    //setECFields();
		    
	    }
		
		public void setECFields() {
			
			EditText fn = ((EditText) getView().findViewById(R.id.ec_first_name));
		    EditText ln = ((EditText) getView().findViewById(R.id.ec_last_name));
		    EditText a = ((EditText) getView().findViewById(R.id.ec_address));
		    EditText c = ((EditText) getView().findViewById(R.id.ec_city));
		    EditText pc = ((EditText) getView().findViewById(R.id.ec_postcode));
		    EditText ph = ((EditText) getView().findViewById(R.id.ec_phone));
		    EditText aph = ((EditText) getView().findViewById(R.id.ec_alternate_phone));
		    EditText r = ((EditText) getView().findViewById(R.id.ec_relationship));
	        
			if (use_nok_as_ec.equals("1")) {
		    	
				editor.putLong("ec_title_pos", settings.getLong("nok_title_pos", 0));
				
				int pos = (int) settings.getLong("nok_title_pos", 0);
			    Log.i("Getting Titles Pos", " - " + pos);
			    titlesSpinner.setSelection(pos, true);
			    
			    editor.putLong("ec_title_pos", pos);
				editor.putString("ec_title", settings.getString("nok_title", ""));
				editor.putString("ec_title_code", settings.getString("nok_title_code", ""));
				editor.putString("ec_first_name", settings.getString("nok_first_name", ""));
				editor.putString("ec_last_name", settings.getString("nok_last_name", ""));
				editor.putString("ec_address", settings.getString("nok_address", ""));
				editor.putString("ec_city", settings.getString("nok_city", ""));
				editor.putString("ec_postcode", settings.getString("nok_postcode", ""));
				editor.putString("ec_phone", settings.getString("nok_phone", ""));
				editor.putString("ec_alternate_phone", settings.getString("nok_alternate_phone", ""));
				editor.putString("ec_relationship", settings.getString("nok_relationship", ""));

				fn.setText(settings.getString("nok_first_name", ""));
				ln.setText(settings.getString("nok_last_name", ""));
				a.setText(settings.getString("nok_address", ""));
				c.setText(settings.getString("nok_city", ""));
				pc.setText(settings.getString("nok_postcode", ""));
				ph.setText(settings.getString("nok_phone", ""));
				aph.setText(settings.getString("nok_alternate_phone", ""));
				r.setText(settings.getString("nok_relationship", ""));

		    	Log.i("ec_first_name", " - " + settings.getString("ec_first_name", ""));
				Log.i("ec_last_name", " - " + settings.getString("ec_last_name", ""));
				Log.i("ec_address", " - " + settings.getString("ec_address", ""));
				Log.i("ec_city", " - " + settings.getString("ec_city", ""));
				Log.i("ec_postcode", " - " + settings.getString("ec_postcode", ""));
				Log.i("ec_phone", " - " + settings.getString("ec_phone", ""));
				Log.i("ec_alternate_phone", " - " + settings.getString("ec_alternate_phone", ""));
				Log.i("ec_relationship", " - " + settings.getString("ec_relationship", ""));
				
				
	    	} else {
	    		
	    		String titles_json = settings.getString("titles_json", "");
			    
			    if (titles_json.length() == 0) {
					getTitles();
			    } else {
			    	loadTitlesJSONResult(titles_json);
			    }
			    
	    		fn.setText(settings.getString("ec_first_name", ""));
				ln.setText(settings.getString("ec_last_name", ""));
				a.setText(settings.getString("ec_address", ""));
				c.setText(settings.getString("ec_city", ""));
				pc.setText(settings.getString("ec_postcode", ""));
				ph.setText(settings.getString("ec_phone", ""));
				aph.setText(settings.getString("ec_alternate_phone", ""));
				r.setText(settings.getString("ec_relationship", ""));
				
				Log.i("ec_first_name", " - " + settings.getString("ec_first_name", ""));
				Log.i("ec_last_name", " - " + settings.getString("ec_last_name", ""));
				Log.i("ec_address", " - " + settings.getString("ec_address", ""));
				Log.i("ec_city", " - " + settings.getString("ec_city", ""));
				Log.i("ec_postcode", " - " + settings.getString("ec_postcode", ""));
				Log.i("ec_phone", " - " + settings.getString("ec_phone", ""));
				Log.i("ec_alternate_phone", " - " + settings.getString("ec_alternate_phone", ""));
				Log.i("ec_relationship", " - " + settings.getString("ec_relationship", ""));
				
	    	}

			editor.commit();
			
		}
		
		public void hideKeyboard() {
	    	InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
	    	View view = getActivity().getCurrentFocus();
	        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
	        //View mainView = getView().findViewById(R.id.pdLayout);
	        //mainView.requestFocus();
	    }
		
		public void setNOKASEC() {
	    	hideKeyboard();
	    	if (use_nok_as_ec.equals("")) {
	    		use_nok_as_ec = "1";
	    		NOKAsECButton.setBackgroundResource(R.drawable.nok_as_ec_checked);
	    	} else {
	    		use_nok_as_ec = "";
	    		NOKAsECButton.setBackgroundResource(R.drawable.nok_as_ec_unchecked);
	    	}
	    	setECFields();
	    }
		
		public boolean validateInput() {
			
			EditText fn = ((EditText) getView().findViewById(R.id.ec_first_name));
		    EditText ln = ((EditText) getView().findViewById(R.id.ec_last_name));
		    EditText ph = ((EditText) getView().findViewById(R.id.ec_phone));
		    
		    String ec_first_name = fn.getText().toString();
			String ec_last_name = ln.getText().toString();
			String ec_phone = ph.getText().toString();
			
			if(ec_first_name.equals("") || ec_last_name.equals("") || ec_phone.equals("")) {
				return false;
			}
			
			return true;
		}

		public void getTitles() {
	    	
	    	Log.i("Getting Titles", "getting titles");
	    	Log.i("Getting Titles URL", "https://secure.docappointment.com.au/apps/patient_details_titles.php?surgery_id="+surgery_id);
	    	AsyncHttpClient client = new AsyncHttpClient();
	        client.get("https://secure.docappointment.com.au/apps/patient_details_titles.php?surgery_id="+surgery_id, new AsyncHttpResponseHandler() {
	            @Override
	            public void onSuccess(String response) {
	            	loadTitlesJSONResult(response);
	            }
	        });
	    	
	    }
	 
		public void loadTitlesJSONResult(String response) {
	    	
	    	editor.putString("titles_json", response);
			editor.commit();
			
	    	ArrayList<HashMap<String, String>> myItems = new ArrayList<HashMap<String, String>>();
	    	
	   	 	JSONObject json = null;
	   	 	
			try {
				json = new JSONObject(response);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}    
	        
	        try{
	        	
	        	titles_array = json.getJSONArray("titles");
	        	//Log.i("Titles", "titles = " + titles_array);
	        	
	        	String ec_title_code = settings.getString("ec_title_code", "");
	        	String ec_title = settings.getString("ec_title", "");
	        	String title = "";
	        	
		        for(int i=0;i<titles_array.length();i++){						
					HashMap<String, String> map = new HashMap<String, String>();	
					JSONObject e = titles_array.getJSONObject(i);
					title = e.getString("title");
					map.put("title",  title);
					
					if (ec_title_code.equals(e.getString("title_code")) || ec_title.equals(e.getString("title"))) {
						final SharedPreferences.Editor editor = settings.edit();
						if (ec_title_code.equals(e.getString("title_code")) ||ec_title.equals(e.getString("title"))) {
							editor.putLong("ec_title_pos", i);
						} else {
							editor.putLong("ec_title_pos", 0);
						}
						editor.commit();
					}
					map.put("title_code",  e.getString("title_code"));
					myItems.add(map);			
				}		
	        }catch(JSONException e)        {
	        	 Log.e("log_tag", "Error parsing data "+e.toString());
	        }
	        
	        titlesSpinner = (Spinner) getView().findViewById(R.id.ec_titles_spinner);
	        SimpleAdapter adapter = new SimpleAdapter(getActivity(), myItems, R.layout.spinner_item,
	                new String[] { "title" }, 
	                new int[] { R.id.spinner_label_1 });
	      
	        titlesSpinner.setAdapter(adapter);
	        
	        int pos = (int) settings.getLong("ec_title_pos", 0);
		    titlesSpinner.setSelection(pos, true);
		   
	       //pd.dismiss();
	        
	    }
}
