package com.FirstAvailable.PatientDetails;


import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

public class PatientDetailsFragment2 extends Fragment {
	
	public static final String PREFS_NAME = "MyPrefsFile";
	public static SharedPreferences settings = null;
	
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {
	        return inflater.inflate(R.layout.patient_details_2, container, false);
	    }
		
		@Override
	    public void onStop() {
	    // TODO Auto-generated method stub
	        super.onStop();

	        Log.i("onStop", "stopping step 1");
	        
	        settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
	        final SharedPreferences.Editor editor = settings.edit();
	        
	        EditText ad1 = ((EditText) getView().findViewById(R.id.address_1));
		    EditText ad2 = ((EditText) getView().findViewById(R.id.address_2));
		    EditText cy = ((EditText) getView().findViewById(R.id.city));
		    EditText pc = ((EditText) getView().findViewById(R.id.postoode));
	        
		    String address_1 = ad1.getText().toString();
			String address_2 = ad2.getText().toString();
			String city = cy.getText().toString();
			String postcode = pc.getText().toString();
			
			editor.putString("address_1", address_1);
			editor.putString("address_2", address_2);
			editor.putString("city", city);
			editor.putString("postcode", postcode);
			
			editor.commit();
			
			Log.i("address_1", " - " + settings.getString("address_1", ""));
			Log.i("address_2", " - " + settings.getString("address_2", ""));
			Log.i("city", " - " + settings.getString("city", ""));
			Log.i("postcode", " - " + settings.getString("postcode", ""));
			 
		}

		@Override
		public void onViewCreated(View view, Bundle savedInstanceState) {
	
		    super.onViewCreated(view, savedInstanceState);
		    
		    settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
			
			String address_1 = settings.getString("address_1", "");
			String address_2 = settings.getString("address_2", "");
			String city = settings.getString("city", "");
			String postcode = settings.getString("postcode", "");
			
			EditText ad1 = ((EditText) getView().findViewById(R.id.address_1));
		    ad1.setText(address_1);
		    ad1.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
		    EditText ad2 = ((EditText) getView().findViewById(R.id.address_2));
		    ad2.setText(address_2);
		    ad2.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
		    EditText cy = ((EditText) getView().findViewById(R.id.city));
		    cy.setText(city);
		    cy.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
		    EditText pc = ((EditText) getView().findViewById(R.id.postoode));
		    pc.setText(postcode);
		    pc.setInputType(InputType.TYPE_CLASS_NUMBER);
		    
	    }
		
		public boolean validateInput() {
			
			EditText pc = ((EditText) getView().findViewById(R.id.postoode));
			
			String postcode = pc.getText().toString();
			
			String postCodeRegex = "^(0[289][0-9]{2})|([1345689][0-9]{3})|(2[0-8][0-9]{2})|(290[0-9])|(291[0-4])|(7[0-4][0-9]{2})|(7[8-9][0-9]{2})$";
			
			if(!postcode.matches(postCodeRegex) && postcode.length() > 0) {
				return false;
			}
			
			return true;
		}

}
