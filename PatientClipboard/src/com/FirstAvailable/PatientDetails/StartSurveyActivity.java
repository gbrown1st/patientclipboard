package com.FirstAvailable.PatientDetails;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class StartSurveyActivity extends Activity implements OnClickListener {
	
	public JSONArray surveys_array;
	public ProgressDialog pd;
	public static final String PREFS_NAME = "MyPrefsFile";
	public static SharedPreferences settings = null;
	public static String surgery_id;
	public Spinner surveysSpinner;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.start_survey);
		
		settings = getSharedPreferences(PREFS_NAME, 0);
		
	    surgery_id = settings.getString("surgery_id", "");
	    
	    View startSurveyBtnClick = findViewById(R.id.start_survey_btn);
	    startSurveyBtnClick.setOnClickListener(this);
		
		surveysSpinner = (Spinner) findViewById(R.id.surveys_spinner);
		
		surveysSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
    	    @Override
    	    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
    
				try {
					JSONObject selectedTitle = surveys_array.getJSONObject(position);
					String survey_title = selectedTitle.getString("survey_title");
					String survey_id = selectedTitle.getString("survey_id");
					SharedPreferences.Editor editor = settings.edit();
					editor = settings.edit();
					editor.putString("survey_title", survey_title);
					editor.putString("survey_id", survey_id);
					
					editor.commit();
					
					Log.i("Object", "survey_title " + survey_title);
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
    	    }
    	    

    	    @Override
    	    public void onNothingSelected(AdapterView<?> parentView) {
    	        // your code here
    	    }

    	});
	    
		
	    
	    getSurveys();
	    
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	
	public void getSurveys() {
    	
    	Log.i("Getting Surveys", "getting surveys");
    	Log.i("Getting Titles URL", "https://secure.docappointment.com.au/apps/patient_surveys.php?surgery_id="+surgery_id);
    	AsyncHttpClient client = new AsyncHttpClient();
        client.get("https://secure.docappointment.com.au/apps/patient_surveys.php?surgery_id="+surgery_id, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String response) {
            	loadSurveysJSONResult(response);
            }
        });
    	
    }
 
    public void loadSurveysJSONResult(String response) {
    	
    	ArrayList<HashMap<String, String>> myItems = new ArrayList<HashMap<String, String>>();
    	
   	 	JSONObject json = null;
   	 	
		try {
			json = new JSONObject(response);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}    
        
        try{
        	
        	surveys_array = json.getJSONArray("surveys");
        	Log.i("Surveys", "Surveys = " + surveys_array);
        	
        	String survey_title;
        	
	        for(int i=0;i<surveys_array.length();i++){						
				HashMap<String, String> map = new HashMap<String, String>();	
				JSONObject e = surveys_array.getJSONObject(i);
				survey_title = e.getString("survey_title");
				map.put("survey_title",  survey_title);
				map.put("survey_id",  e.getString("survey_id"));
				myItems.add(map);			
			}		
        }catch(JSONException e)        {
        	 Log.e("log_tag", "Error parsing data "+e.toString());
        }
        
        surveysSpinner = (Spinner) findViewById(R.id.surveys_spinner);
        SimpleAdapter adapter = new SimpleAdapter(this, myItems, R.layout.spinner_item,
                new String[] { "survey_title" }, 
                new int[] { R.id.spinner_label_1 });
      
        surveysSpinner.setAdapter(adapter);
        
       //pd.dismiss();
        
    }

    @Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		
		case R.id.start_survey_btn:
			Intent sa = new Intent(this, SurveyActivity.class);  
			startActivity(sa);
		break;
			
		}
	}
}
